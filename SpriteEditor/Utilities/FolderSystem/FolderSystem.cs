﻿using System;
using System.Drawing;

namespace SpriteEditor.FolderSystem
{
    public class FileManager
    {
        public static void ExportFile(Image imageToExport, string exportLocation)
        {
            //Save the image to disk
            imageToExport.Save(exportLocation);

            Console.WriteLine($"Exported image to {exportLocation}");
        }
    }
}