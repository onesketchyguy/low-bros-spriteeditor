﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Security.Permissions;
using System.Windows.Forms;

namespace SpriteEditor.Updates
{
    [PermissionSet(SecurityAction.LinkDemand, Action = SecurityAction.Demand, Name = "FullTrust")]
    public partial class UpdateChecker : Form
    {
        private static string DownloadURL = "https://gitlab.com/onesketchyguy/better-sprite-editor/-/archive/master/better-sprite-editor-master.zip";
        private static string VersionCodeLocation = "https://gitlab.com/onesketchyguy/better-sprite-editor/raw/master/Resources/version.txt";

        private static string DownloadedFileName = "Master.zip";
        private static string DefaultInstallLocation = Application.StartupPath;

        private static Assembly currentAssembly;
        private static string archivePath;

        private static string currentVersion
        {
            get
            {
                return File.ReadAllText("Resources/version.txt");
            }
        }

        public static string GetAppDirectory()
        {
            return Path.GetDirectoryName(currentAssembly.Location);
        }

        public static string GetAppName()
        {
            return Path.GetFileNameWithoutExtension(currentAssembly.Location);
        }

        public static string GetAppExtention()
        {
            return Path.GetExtension(currentAssembly.Location);
        }

        public static string GetGlobalVersion()
        {
            string url = VersionCodeLocation;

            WebRequest request = WebRequest.Create(url);
            WebResponse response = request.GetResponse();
            StreamReader streamReader = new StreamReader(response.GetResponseStream());

            string latestVersion = streamReader.ReadToEnd();

            streamReader.Close();
            streamReader.Dispose();

            return latestVersion;
        }

        public static WebClient Client { get; private set; }

        public static string InstallLocation
        {
            get
            {
                return Application.StartupPath;
            }
        }

        public UpdateChecker()
        {
            InitializeComponent();

            currentAssembly = Assembly.GetEntryAssembly();
            if (currentAssembly == null)
                currentAssembly = Assembly.GetCallingAssembly();

            archivePath = Path.Combine(Application.StartupPath, $"{GetAppName()}_OldVersion{Application.StartupPath}");
        }

        private void UpdateChecker_Load(object sender, EventArgs e)
        {
            // Check for updates first

            var versionString = GetGlobalVersion().Split('=').LastOrDefault();
            var currentString = currentVersion.Split('=').LastOrDefault();

            var globalVers = int.Parse(versionString);
            var localVers = int.Parse(currentString);

            if (globalVers <= localVers)
            {
                MessageBox.Show($"Your version:{localVers} is up to date.", "Up to date.");

                Close();
                return;
            }
            else
            {
                MessageBox.Show($"Version: {globalVers} available now.\n Downloading...", "Update available!");
            }

            // Download new update

            if (System.IO.File.Exists(archivePath))
                System.IO.File.Delete(archivePath);

            Client = new WebClient();
            Client.DownloadFileCompleted += Client_DownloadFileCompleted;  //Extract file when it's ready

            Uri uri = new Uri(DownloadURL);

            try
            {
                Client.DownloadFileAsync(uri, $"{DownloadedFileName}");
            }
            catch
            {
                MessageBox.Show("Unable to download files...");
            }
        }

        private static void Client_DownloadFileCompleted(object sender, AsyncCompletedEventArgs e)
        {
            Debug.Print($"Downloaded update complete called.");

            Client.Dispose();

            LoadUpdate();
        }

        private static void LoadUpdate()
        {
            Debug.Print($"Load update called.");

            DirectoryInfo directorySelected = new DirectoryInfo(Path.GetDirectoryName(currentAssembly.Location));

            FileInfo[] filesToDecompress = directorySelected.GetFiles("*.zip");

            //Check if decompression was successful
            if (filesToDecompress != null && Unzip(filesToDecompress.FirstOrDefault()) == true)
            {
                //Move Files
                MoveNewFoldersIntoPlace(DownloadedFileName);

                //Remove old files
                RemoveOldVersion();
            }
            else
            {
                MessageBox.Show($"Failed to unzip files.\nExiting program.", "Failure to install!", MessageBoxButtons.OK, MessageBoxIcon.Error);

                Environment.Exit(2);
            }

            MessageBox.Show($"Completed update. Restart of launcher required...\n\nIf you find a \"better-sprite-editor-master\" folder in your directory, the update failed to replace the local files. Please ensure that you have replaced the project files with the files within.");

            Environment.Exit(2);
        }

        private static bool Unzip(FileInfo fileToDecompress)
        {
            Debug.Print($"Unzip update called.");

            bool success = false;

            using (ZipArchive zip = ZipFile.OpenRead(fileToDecompress.FullName))
            {
                try
                {
                    zip.ExtractToDirectory(GetAppDirectory());

                    success = true;
                }
                catch
                {
                    try
                    {
                        Directory.Delete($"{GetAppDirectory()}\\{DownloadedFileName}", true);

                        zip.ExtractToDirectory(GetAppDirectory());

                        success = true;
                    }
                    catch
                    {
                        //MessageBox.Show("Unable to delete zip file...\nA manual deletion will be required to finish update.", "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                        //Environment.Exit(1);
                    }
                }
            }

            //Delete old folder.
            try
            {
                System.IO.File.Delete($"{GetAppDirectory()}/{DownloadedFileName}");

                success = true;
            }
            catch
            {
                //MessageBox.Show("Unable to cleanup zip file...", "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

            return success;
        }

        private static void RemoveOldVersion()
        {
            string[] files = Directory.GetFiles(GetAppDirectory());
            foreach (string file in files)
            {
                string name = Path.GetFileName(file);

                try
                {
                    System.IO.File.Delete(file);
                }
                catch
                {
                    if (file == $"Resources/version.txt")
                    {
                        string c = System.IO.File.ReadAllText($"{GetAppDirectory()}/{DownloadedFileName}/Resources/version.txt");

                        System.IO.File.WriteAllText(file, c);

                        continue;
                    }

                    if (file.Contains(GetAppName())) continue;
                }
            }
        }

        private static void MoveNewFoldersIntoPlace(string fileName)
        {
            string dir = $"{GetAppDirectory()}/{fileName}";

            CopyDir(GetAppDirectory(), GetAppDirectory());
        }

        public static void CopyDir(string sourceFolder, string destFolder)
        {
            if (!Directory.Exists(destFolder))
                Directory.CreateDirectory(destFolder);

            // Get Files & Copy
            string[] files = Directory.GetFiles(sourceFolder);
            foreach (string file in files)
            {
                if (file == string.Empty || Path.GetFileName(file) == null) continue;

                string name = Path.GetFileName(file);

                // ADD Unique File Name Check to Below!!!!
                string dest = Path.Combine(destFolder, name);

                try
                {
                    System.IO.File.Copy(file, dest, true);

                    System.IO.File.Delete(file);
                }
                catch
                {
                    if (file.Contains(GetAppName()))
                    {
                        try
                        {
                            string app = Path.GetFileName(currentAssembly.Location);

                            System.IO.File.Move(app, archivePath);

                            System.IO.File.Copy(file, app, true);
                        }
                        catch
                        {
                            //MessageBox.Show($"Unable to replace file: {file}...\nWill continue regardless.", "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                    }
                    else
                        MessageBox.Show($"Unable to replace file: {file}...\nWill continue regardless.", "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }

            // Get dirs recursively and copy files
            string[] folders = Directory.GetDirectories(sourceFolder);
            foreach (string folder in folders)
            {
                string name = Path.GetFileName(folder);
                string dest = Path.Combine(destFolder, name);
                CopyDir(folder, dest);
            }
        }

        public static void CleanUp()
        {
            //Remove all temp created files
            if (Directory.Exists($"{Path.GetDirectoryName(currentAssembly.Location)}/{DownloadedFileName}.zip"))
            {
                Directory.Delete($"{Path.GetDirectoryName(currentAssembly.Location)}/{DownloadedFileName}.zip", true);
            }

            if (System.IO.File.Exists(archivePath))
                System.IO.File.Delete(archivePath);
        }
    }
}