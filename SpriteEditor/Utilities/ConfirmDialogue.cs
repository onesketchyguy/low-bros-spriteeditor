﻿using System;
using System.Windows.Forms;

namespace SpriteEditor.Utilities
{
    public partial class ConfirmDialogue : Form
    {
        public bool Confirmed = false;

        public ConfirmDialogue(string startUptext = "Delete?")
        {
            InitializeComponent();

            TextObject.Text = startUptext;
        }

        private void ConfirmButton_Click(object sender, EventArgs e)
        {
            Confirmed = true;
            Close();
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}