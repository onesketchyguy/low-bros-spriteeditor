﻿using SpriteEditor.Utilities;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace SpriteEditor.Prefrences
{
    public partial class PrefrencesDialog : Form
    {
        public PrefrencesDialog()
        {
            InitializeComponent();

            GridColorA.BackColor = Settings.GridColorA;
            GridColorB.BackColor = Settings.GridColorB;
            LineGridColor.BackColor = Settings.LineGridColor;
            GridScroll.Value = Settings.GridScale;
            GridScroll.Maximum = (Settings.canvas_width / 2) - 1;
        }

        private Color ChangeColor()
        {
            var color = Color.Empty;

            // Open Color Dialogue
            using (var dialog = new ColorDialog())
            {
                DialogResult result = dialog.ShowDialog();

                color = dialog.Color;

                dialog.Dispose();
            }

            return color;
        }

        private void GridColorA_Click(object sender, EventArgs e)
        {
            Settings.GridColorA = ChangeColor();
            GridColorA.BackColor = Settings.GridColorA;
        }

        private void GridColorB_Click(object sender, EventArgs e)
        {
            Settings.GridColorB = ChangeColor();
            GridColorB.BackColor = Settings.GridColorB;
        }

        private void LineGridColor_Click(object sender, EventArgs e)
        {
            Settings.LineGridColor = ChangeColor();
            LineGridColor.BackColor = Settings.LineGridColor;
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            // Save the changes
            PrefrencesManager.CreateFile();
        }

        private void GridScale_Scroll(object sender, EventArgs e)
        {
            var val = GridScroll.Value + 1;

            Settings.GridScale = (byte)val;
        }
    }
}