﻿namespace SpriteEditor.Prefrences
{
    partial class PrefrencesDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.GridColorA = new System.Windows.Forms.PictureBox();
            this.GridColorAText = new System.Windows.Forms.Label();
            this.GridColorB = new System.Windows.Forms.PictureBox();
            this.GridColorBText = new System.Windows.Forms.Label();
            this.LineGridColor = new System.Windows.Forms.PictureBox();
            this.LineGridColorText = new System.Windows.Forms.Label();
            this.GridScroll = new System.Windows.Forms.TrackBar();
            this.GridScaleLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.GridColorA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridColorB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LineGridColor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridScroll)).BeginInit();
            this.SuspendLayout();
            // 
            // GridColorA
            // 
            this.GridColorA.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.GridColorA.Location = new System.Drawing.Point(12, 12);
            this.GridColorA.Name = "GridColorA";
            this.GridColorA.Size = new System.Drawing.Size(35, 35);
            this.GridColorA.TabIndex = 1;
            this.GridColorA.TabStop = false;
            this.GridColorA.Click += new System.EventHandler(this.GridColorA_Click);
            // 
            // GridColorAText
            // 
            this.GridColorAText.AutoSize = true;
            this.GridColorAText.Location = new System.Drawing.Point(53, 34);
            this.GridColorAText.Name = "GridColorAText";
            this.GridColorAText.Size = new System.Drawing.Size(62, 13);
            this.GridColorAText.TabIndex = 2;
            this.GridColorAText.Text = "Grid color A";
            // 
            // GridColorB
            // 
            this.GridColorB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.GridColorB.Location = new System.Drawing.Point(12, 53);
            this.GridColorB.Name = "GridColorB";
            this.GridColorB.Size = new System.Drawing.Size(35, 35);
            this.GridColorB.TabIndex = 1;
            this.GridColorB.TabStop = false;
            this.GridColorB.Click += new System.EventHandler(this.GridColorB_Click);
            // 
            // GridColorBText
            // 
            this.GridColorBText.AutoSize = true;
            this.GridColorBText.Location = new System.Drawing.Point(53, 75);
            this.GridColorBText.Name = "GridColorBText";
            this.GridColorBText.Size = new System.Drawing.Size(62, 13);
            this.GridColorBText.TabIndex = 2;
            this.GridColorBText.Text = "Grid color B";
            // 
            // LineGridColor
            // 
            this.LineGridColor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.LineGridColor.Location = new System.Drawing.Point(12, 94);
            this.LineGridColor.Name = "LineGridColor";
            this.LineGridColor.Size = new System.Drawing.Size(35, 35);
            this.LineGridColor.TabIndex = 1;
            this.LineGridColor.TabStop = false;
            this.LineGridColor.Click += new System.EventHandler(this.LineGridColor_Click);
            // 
            // LineGridColorText
            // 
            this.LineGridColorText.AutoSize = true;
            this.LineGridColorText.Location = new System.Drawing.Point(53, 116);
            this.LineGridColorText.Name = "LineGridColorText";
            this.LineGridColorText.Size = new System.Drawing.Size(73, 13);
            this.LineGridColorText.TabIndex = 2;
            this.LineGridColorText.Text = "Line grid color";
            // 
            // GridScroll
            // 
            this.GridScroll.LargeChange = 4;
            this.GridScroll.Location = new System.Drawing.Point(12, 135);
            this.GridScroll.Maximum = 63;
            this.GridScroll.Name = "GridScroll";
            this.GridScroll.Size = new System.Drawing.Size(201, 45);
            this.GridScroll.TabIndex = 3;
            this.GridScroll.Scroll += new System.EventHandler(this.GridScale_Scroll);
            // 
            // GridScaleLabel
            // 
            this.GridScaleLabel.AutoSize = true;
            this.GridScaleLabel.Location = new System.Drawing.Point(53, 167);
            this.GridScaleLabel.Name = "GridScaleLabel";
            this.GridScaleLabel.Size = new System.Drawing.Size(54, 13);
            this.GridScaleLabel.TabIndex = 4;
            this.GridScaleLabel.Text = "Grid scale";
            // 
            // PrefrencesDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(225, 216);
            this.Controls.Add(this.GridScaleLabel);
            this.Controls.Add(this.GridScroll);
            this.Controls.Add(this.LineGridColorText);
            this.Controls.Add(this.GridColorBText);
            this.Controls.Add(this.GridColorAText);
            this.Controls.Add(this.LineGridColor);
            this.Controls.Add(this.GridColorB);
            this.Controls.Add(this.GridColorA);
            this.Name = "PrefrencesDialog";
            this.Text = "PrefrencesManager";
            ((System.ComponentModel.ISupportInitialize)(this.GridColorA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridColorB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LineGridColor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridScroll)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox GridColorA;
        private System.Windows.Forms.Label GridColorAText;
        private System.Windows.Forms.PictureBox GridColorB;
        private System.Windows.Forms.Label GridColorBText;
        private System.Windows.Forms.PictureBox LineGridColor;
        private System.Windows.Forms.Label LineGridColorText;
        private System.Windows.Forms.TrackBar GridScroll;
        private System.Windows.Forms.Label GridScaleLabel;
    }
}