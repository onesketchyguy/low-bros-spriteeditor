﻿using SpriteEditor.Utilities;
using System.Drawing;
using System.IO;
using System.Linq;

namespace SpriteEditor.Prefrences
{
    public class PrefrencesManager
    {
        private static readonly string PrefrencesLocation = "Resources/Prefrences.txt";

        private const char DataSeperator = '-';

        public delegate void DataChanged();

        public static DataChanged PreferencesChanged;

        public static void CreateFile()
        {
            // Create a new prefrences file
            var content =
                $"lineGridColor.R:{Settings.LineGridColor.R}{DataSeperator}\n" +
                $"lineGridColor.G:{Settings.LineGridColor.G}{DataSeperator}\n" +
                $"lineGridColor.B:{Settings.LineGridColor.B}{DataSeperator}\n" +
                $"linGridOn:{Settings.DrawLineGrid}{DataSeperator}\n" +
                $"gridColorA.R:{Settings.GridColorA.R}{DataSeperator}\n" +
                $"gridColorA.G:{Settings.GridColorA.G}{DataSeperator}\n" +
                $"gridColorA.B:{Settings.GridColorA.B}{DataSeperator}\n" +
                $"gridColorB.R:{Settings.GridColorB.R}{DataSeperator}\n" +
                $"gridColorB.G:{Settings.GridColorB.G}{DataSeperator}\n" +
                $"gridColorB.B:{Settings.GridColorB.B}{DataSeperator}\n" +
                $"gridScale:{Settings.GridScale}{DataSeperator}\n";

            File.WriteAllText(PrefrencesLocation, content);

            if (PreferencesChanged != null)
                PreferencesChanged.Invoke();
        }

        public static void LoadFile()
        {
            // Load the prefrences file if one exists
            if (File.Exists(PrefrencesLocation) == false)
            {
                // Create a new file
                CreateFile();

                return;
            }

            // Load all information to the settings manager.
            var prefrences = File.ReadAllText(PrefrencesLocation);
            var content = prefrences.Split(DataSeperator);

            byte lineGridColorR = 0;
            byte lineGridColorG = 0;
            byte lineGridColorB = 0;
            var activateLineGrid = false;

            byte gridColorAR = 0;
            byte gridColorAG = 0;
            byte gridColorAB = 0;

            byte gridColorBR = 0;
            byte gridColorBG = 0;
            byte gridColorBB = 0;

            byte gridScale = 0;

            foreach (var piece in content)
            {
                if (piece.Contains("lineGridColor"))
                {
                    var bit = piece.Split(':').LastOrDefault();

                    if (piece.Contains(".R"))
                        lineGridColorR = byte.Parse(bit);
                    if (piece.Contains(".G"))
                        lineGridColorG = byte.Parse(bit);
                    if (piece.Contains(".B"))
                        lineGridColorB = byte.Parse(bit);
                }
                if (piece.Contains("linGridOn"))
                {
                    var bit = piece.Split(':').LastOrDefault();

                    bool.TryParse(bit, out activateLineGrid);
                }

                if (piece.Contains("gridColorA"))
                {
                    var bit = piece.Split(':').LastOrDefault();

                    if (piece.Contains(".R"))
                        gridColorAR = byte.Parse(bit);
                    if (piece.Contains(".G"))
                        gridColorAG = byte.Parse(bit);
                    if (piece.Contains(".B"))
                        gridColorAB = byte.Parse(bit);
                }
                if (piece.Contains("gridColorB"))
                {
                    var bit = piece.Split(':').LastOrDefault();

                    if (piece.Contains(".R"))
                        gridColorBR = byte.Parse(bit);
                    if (piece.Contains(".G"))
                        gridColorBG = byte.Parse(bit);
                    if (piece.Contains(".B"))
                        gridColorBB = byte.Parse(bit);
                }

                if (piece.Contains("gridScale"))
                {
                    var bit = piece.Split(':').LastOrDefault();
                    gridScale = byte.Parse(bit);
                }
            }

            Settings.DrawLineGrid = activateLineGrid;
            Settings.LineGridColor = Color.FromArgb(lineGridColorR, lineGridColorG, lineGridColorB);

            Settings.GridColorA = Color.FromArgb(gridColorAR, gridColorAG, gridColorAB);
            Settings.GridColorB = Color.FromArgb(gridColorBR, gridColorBG, gridColorBB);

            if (gridScale > 0)
                Settings.GridScale = gridScale;
        }
    }
}