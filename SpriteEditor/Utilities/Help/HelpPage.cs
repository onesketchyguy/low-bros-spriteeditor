﻿using System.Windows.Forms;

namespace SpriteEditor.Help
{
    public partial class HelpPage : Form
    {
        public HelpPage()
        {
            InitializeComponent();
        }

        private void Link_Clicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            // Link to the site we would like to navigate to
            var link = "https://www.lowbros.us/editor";

            // Start the weblink
            System.Diagnostics.Process.Start(link);
        }
    }
}