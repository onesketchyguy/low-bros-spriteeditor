﻿namespace SpriteEditor.Help
{
    partial class HelpPage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HelpPage));
            this.Image = new System.Windows.Forms.PictureBox();
            this.link = new System.Windows.Forms.LinkLabel();
            this.InfoDump = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.Image)).BeginInit();
            this.SuspendLayout();
            // 
            // Image
            // 
            this.Image.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Image.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Image.BackgroundImage")));
            this.Image.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Image.InitialImage = ((System.Drawing.Image)(resources.GetObject("Image.InitialImage")));
            this.Image.Location = new System.Drawing.Point(0, 0);
            this.Image.Name = "Image";
            this.Image.Size = new System.Drawing.Size(751, 371);
            this.Image.TabIndex = 0;
            this.Image.TabStop = false;
            // 
            // link
            // 
            this.link.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.link.AutoSize = true;
            this.link.Location = new System.Drawing.Point(656, 410);
            this.link.Name = "link";
            this.link.Size = new System.Drawing.Size(83, 13);
            this.link.TabIndex = 1;
            this.link.TabStop = true;
            this.link.Text = "MoreInformation";
            this.link.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.Link_Clicked);
            // 
            // InfoDump
            // 
            this.InfoDump.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.InfoDump.AutoSize = true;
            this.InfoDump.Location = new System.Drawing.Point(8, 374);
            this.InfoDump.Name = "InfoDump";
            this.InfoDump.Size = new System.Drawing.Size(403, 13);
            this.InfoDump.TabIndex = 3;
            this.InfoDump.Text = "The Better Sprite editor was designed, concieved and programmed by Forrest Lowe.";
            // 
            // HelpPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(751, 432);
            this.Controls.Add(this.InfoDump);
            this.Controls.Add(this.link);
            this.Controls.Add(this.Image);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "HelpPage";
            this.Text = "Help";
            ((System.ComponentModel.ISupportInitialize)(this.Image)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox Image;
        private System.Windows.Forms.LinkLabel link;
        private System.Windows.Forms.Label InfoDump;
    }
}