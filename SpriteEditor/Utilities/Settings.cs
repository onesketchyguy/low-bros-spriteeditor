﻿using System.Drawing;
using System.Windows.Forms;

namespace SpriteEditor.Utilities
{
    public class Settings
    {
        public static Cursor Pen => new Cursor("Resources/pen.cur");

        public static Cursor Eracer => new Cursor("Resources/eraser.cur");

        public static Cursor Bucket => new Cursor("Resources/bucket.cur");

        public static Cursor Sponge => new Cursor("Resources/sponge.cur");

        public static Cursor Ruler => Cursors.IBeam; // Temporary, TO BE REPLACED!

        public static byte GridScale = 1;

        public static int canvas_width = 16, canvas_height = 16;

        public static bool DrawLineGrid = false;

        public static Color LineGridColor = Color.HotPink;

        public static Color GridColorA = Color.LightSalmon;
        public static Color GridColorB = Color.LightBlue;

        public static bool mirroring_horizontally;
        public static bool mirroring_vertically;
    }
}