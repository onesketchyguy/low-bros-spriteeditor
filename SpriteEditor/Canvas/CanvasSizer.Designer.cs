﻿namespace SpriteEditor.Canvas
{
    partial class CanvasSizer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CanvasSizer));
            this.doneButton = new System.Windows.Forms.Button();
            this.SizeX = new System.Windows.Forms.TextBox();
            this.SizeY = new System.Windows.Forms.TextBox();
            this.xlabel = new System.Windows.Forms.Label();
            this.ylabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // doneButton
            // 
            this.doneButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.doneButton.AutoSize = true;
            this.doneButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.doneButton.Location = new System.Drawing.Point(107, 12);
            this.doneButton.MaximumSize = new System.Drawing.Size(45, 25);
            this.doneButton.MinimumSize = new System.Drawing.Size(43, 23);
            this.doneButton.Name = "doneButton";
            this.doneButton.Size = new System.Drawing.Size(43, 23);
            this.doneButton.TabIndex = 0;
            this.doneButton.Text = "Done";
            this.doneButton.UseVisualStyleBackColor = true;
            this.doneButton.Click += new System.EventHandler(this.DoneButton_Click);
            // 
            // SizeX
            // 
            this.SizeX.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.SizeX.Location = new System.Drawing.Point(12, 6);
            this.SizeX.MaxLength = 4;
            this.SizeX.Name = "SizeX";
            this.SizeX.Size = new System.Drawing.Size(37, 20);
            this.SizeX.TabIndex = 1;
            this.SizeX.Tag = "";
            this.SizeX.Text = "16";
            this.SizeX.TextChanged += new System.EventHandler(this.SizeX_TextChanged);
            // 
            // SizeY
            // 
            this.SizeY.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.SizeY.Location = new System.Drawing.Point(12, 32);
            this.SizeY.MaxLength = 4;
            this.SizeY.Name = "SizeY";
            this.SizeY.Size = new System.Drawing.Size(37, 20);
            this.SizeY.TabIndex = 1;
            this.SizeY.Text = "16";
            this.SizeY.TextChanged += new System.EventHandler(this.SizeX_TextChanged);
            // 
            // xlabel
            // 
            this.xlabel.AutoSize = true;
            this.xlabel.Location = new System.Drawing.Point(55, 9);
            this.xlabel.Name = "xlabel";
            this.xlabel.Size = new System.Drawing.Size(14, 13);
            this.xlabel.TabIndex = 2;
            this.xlabel.Text = "X";
            // 
            // ylabel
            // 
            this.ylabel.AutoSize = true;
            this.ylabel.Location = new System.Drawing.Point(55, 35);
            this.ylabel.Name = "ylabel";
            this.ylabel.Size = new System.Drawing.Size(14, 13);
            this.ylabel.TabIndex = 2;
            this.ylabel.Text = "Y";
            // 
            // CanvasSizer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(162, 65);
            this.Controls.Add(this.ylabel);
            this.Controls.Add(this.xlabel);
            this.Controls.Add(this.SizeY);
            this.Controls.Add(this.SizeX);
            this.Controls.Add(this.doneButton);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CanvasSizer";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Canvas Size";
            this.TopMost = true;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button doneButton;
        private System.Windows.Forms.TextBox SizeX;
        private System.Windows.Forms.TextBox SizeY;
        private System.Windows.Forms.Label xlabel;
        private System.Windows.Forms.Label ylabel;
    }
}