﻿using SpriteEditor.Highlight;
using SpriteEditor.Utilities;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace SpriteEditor.Canvas
{
    public class CanvasPainter
    {
        // Set size
        private static int canvas_width = 16, canvas_height = 16;

        private static PictureBox CanvasArea;

        private static Bitmap grid;

        public CanvasPainter(int size_x, int size_y, PictureBox image, Bitmap bitmap = null)
        {
            canvas_width = size_x;
            canvas_height = size_y;

            var bmp = new Bitmap(canvas_width, canvas_height);

            if (bitmap != null)
            {
                for (int x = 0; x < size_x; x++)
                {
                    for (int y = 0; y < size_y; y++)
                    {
                        if (x >= bitmap.Width || y >= bitmap.Height) continue;
                        if (x > size_x || y > size_y) continue;

                        if (bitmap.GetPixel(x, y).A > 0) // There is a color here
                        {
                            bmp.SetPixel(x, y, bitmap.GetPixel(x, y));
                        }
                    }
                }
            }

            image.Image = bmp;

            CanvasArea = image;

            CanvasArea.Invalidated += CanvasArea_Invalidated;

            CanvasArea.Invalidate();

            grid = GetGrid(canvas_width, canvas_height);

            Prefrences.PrefrencesManager.PreferencesChanged += () =>
            {
                grid = GetGrid(Settings.canvas_width, Settings.canvas_height);
            };
        }

        public static void ClearCanvas()
        {
            // Clear all pixels
            for (int drawY = 0; drawY < canvas_height; drawY++)
            {
                for (int drawX = 0; drawX < canvas_width; drawX++)
                {
                    DrawPixelWithoutUpdate(drawX, drawY, Color.Empty);
                }
            }

            //Update the canvas
            UpdateCanvas();
        }

        public static Bitmap MirrorHorizontally(Bitmap bitmap)
        {
            var flipped = new Bitmap(bitmap);
            flipped.RotateFlip(RotateFlipType.RotateNoneFlipX);

            var toReturn = bitmap;

            for (int x = 0; x < bitmap.Width; x++)
            {
                for (int y = 0; y < bitmap.Height; y++)
                {
                    var color = bitmap.GetPixel(x, y);

                    if (x >= bitmap.Width / 2)
                    {
                        color = flipped.GetPixel(x, y);
                    }

                    // Set pixels
                    toReturn.SetPixel(x, y, color);
                }
            }

            return toReturn;
        }

        public static Bitmap MirrorVertically(Bitmap bitmap)
        {
            var flipped = new Bitmap(bitmap);
            flipped.RotateFlip(RotateFlipType.RotateNoneFlipY);

            var toReturn = bitmap;

            for (int x = 0; x < bitmap.Width; x++)
            {
                for (int y = 0; y < bitmap.Height; y++)
                {
                    var color = bitmap.GetPixel(x, y);

                    if (y >= bitmap.Height / 2)
                    {
                        color = flipped.GetPixel(x, y);
                    }

                    // Set pixels
                    toReturn.SetPixel(x, y, color);
                }
            }

            return toReturn;
        }

        public static void UpdateCanvas()
        {
            if (drawing) return;
            drawing = true;

            CanvasArea.Invalidate();
        }

        private static void CanvasArea_Invalidated(object sender, InvalidateEventArgs e)
        {
            drawing = false;

            grid = GetGrid(Settings.canvas_width, Settings.canvas_height);
        }

        private static bool drawing;

        public static void DrawPixelAt(int x, int y, Color color)
        {
            if (x >= canvas_width || x < 0 || y >= canvas_height || y < 0) return;

            // Settup bitmap
            var bmp = (Bitmap)CanvasArea.Image;
            var existingColor = GetPixelAt(x, y);

            if (color.A == 0)
            {
                ClearPixelAt(x, y);

                return;
            }

            if (color.A < 255)
            {
                // Mixable color
                if (existingColor.A > 0)
                {
                    // Mix colors
                    var existingAlpha = existingColor.A;
                    var newColorAlpha = color.A;

                    double amount;

                    if (newColorAlpha > existingAlpha)
                    {
                        amount = (newColorAlpha - existingAlpha) / newColorAlpha;
                    }
                    else
                    {
                        amount = (existingAlpha - newColorAlpha) / existingAlpha;
                    }

                    var newColor = Blend(color, existingColor, amount);

                    bmp.SetPixel(x, y, newColor);
                }
            }
            else
            {
                // Solid color
                bmp.SetPixel(x, y, color);
            }

            // Update the canvas
            UpdateCanvas();
        }

        public static Color Blend(Color color, Color backColor, double amount)
        {
            byte r = (byte)((color.R * amount) + backColor.R * (1 - amount));
            byte g = (byte)((color.G * amount) + backColor.G * (1 - amount));
            byte b = (byte)((color.B * amount) + backColor.B * (1 - amount));
            return Color.FromArgb(r, g, b);
        }

        public static void DrawPixelWithoutUpdate(int x, int y, Color color)
        {
            if (x >= canvas_width || x < 0 || y >= canvas_height || y < 0) return;

            // Settup bitmap
            var bmp = (Bitmap)CanvasArea.Image;

            bmp.SetPixel(x, y, color);

            // Load the bmp into the image region
            CanvasArea.Image = bmp;
        }

        public static Color GetPixelAt(int x, int y)
        {
            if (x >= canvas_width || x < 0 || y >= canvas_height || y < 0) return Color.FromArgb(0, 0, 0, 0);

            // Settup bitmap
            var bmp = (Bitmap)CanvasArea.Image;

            // Return the color
            return bmp.GetPixel(x, y);
        }

        public static void ClearPixelAt(int x, int y)
        {
            if (x >= canvas_width || x < 0 || y >= canvas_height || y < 0) return;

            // Settup bitmap
            var bmp = (Bitmap)CanvasArea.Image;

            bmp.SetPixel(x, y, Color.FromArgb(0, 0, 0, 0));

            // Load the bmp into the image region
            CanvasArea.Image = bmp;

            UpdateCanvas();
        }

        public static void ClearPixels(Vector2[] vector2s)
        {
            // Settup bitmap
            var bmp = (Bitmap)CanvasArea.Image;

            foreach (var item in vector2s)
            {
                int x = (int)item.x;
                int y = (int)item.y;

                if (x >= canvas_width || x < 0 || y >= canvas_height || y < 0) continue;

                bmp.SetPixel(x, y, Color.FromArgb(0, 0, 0, 0));
            }

            // Load the bmp into the image region
            CanvasArea.Image = bmp;

            UpdateCanvas();
        }

        public static List<Vector2> GetNeighboringPixels(int x, int y)
        {
            var positions = new List<Vector2>();

            var distance = 1;
            /*
            // Add from top left to bottom right
            for (int drawY = y - distance; drawY <= y + distance; drawY++)
            {
                for (int drawX = x - distance; drawX <= x + distance; drawX++)
                {
                    var pos = new Vector2(drawX, drawY);

                    if (pos.x < 0 || pos.x >= canvas_width || pos.y < 0 || pos.y >= canvas_height) continue;

                    positions.Add(pos);
                }
            }*/

            //Add up down left and right
            for (int drawY = y - distance; drawY <= y + distance; drawY++)
            {
                var pos = new Vector2(x, drawY);

                if (pos.x < 0 || pos.x >= canvas_width || pos.y < 0 || pos.y >= canvas_height) continue;

                positions.Add(pos);
            }

            for (int drawX = x - distance; drawX <= x + distance; drawX++)
            {
                var pos = new Vector2(drawX, y);

                if (pos.x < 0 || pos.x >= canvas_width || pos.y < 0 || pos.y >= canvas_height
                    || positions.Contains(pos)) continue;

                positions.Add(pos);
            }

            return positions;
        }

        public static void FillAreaFrom(int x, int y, Color fillColor)
        {
            // Settup bitmap
            var bmp = (Bitmap)CanvasArea.Image;

            // Get the pixels
            var colorParent = GetPixelAt(x, y);

            var openSet = new HashSet<Vector2>() { new Vector2(x, y) };
            var closedSet = new HashSet<Vector2>();

            var positions = new List<Vector2>();

            while (openSet.Count > 0)
            {
                // Go through each pixel neighbor and find the same colors
                var pos = openSet.FirstOrDefault();

                if (GetPixelAt((int)pos.x, (int)pos.y) == colorParent)
                {
                    var neighbors = GetNeighboringPixels((int)pos.x, (int)pos.y);

                    foreach (var item in neighbors)
                    {
                        if (closedSet.Contains(item) == false && GetPixelAt((int)item.x, (int)item.y) == colorParent)
                        {
                            positions.Add(item);

                            openSet.Add(item);
                        }
                    }
                }

                closedSet.Add(pos);
                openSet.Remove(pos);
            }

            foreach (var item in positions) bmp.SetPixel((int)item.x, (int)item.y, fillColor);

            CanvasArea.Image = bmp;

            UpdateCanvas();
        }

        /// <summary>
        /// Paint the canvas
        /// </summary>
        /// <param name="bmp"></param>
        /// <param name="canvas"></param>
        public static void PaintCanvas(Bitmap bmp, Graphics canvas, Vector2 currentMousePos, int mouse_y, int mouse_x, Color currentPixelColor)
        {
            // Cancel statements
            if (bmp == null) return;

            // Whipe the canvas clean
            canvas.Clear(Color.Empty);

            var filling = 0.1f;

            // Create a scale and add the difference for the gaps
            var scale = new SizeF((CanvasArea.Width / Settings.canvas_width) + filling, (CanvasArea.Height / Settings.canvas_height) + filling);

            void DrawPixel(PointF pos, SolidBrush color, float scaleMod = 0)
            {
                var finalColor = color;

                var x = (pos.X / CanvasArea.Width) * Settings.canvas_width;
                var y = (pos.Y / CanvasArea.Height) * Settings.canvas_height;

                if ((Settings.mirroring_horizontally && x >= Settings.canvas_width / 2) || (Settings.mirroring_vertically && y >= Settings.canvas_height / 2))
                {
                    finalColor = new SolidBrush(Color.FromArgb(color.Color.A / 2, finalColor.Color));
                }

                var finalScale = new SizeF(scale.Width + scaleMod, scale.Height + scaleMod);

                var difference = scaleMod > 0 ? scaleMod / 2 : 0;

                var finalPos = new PointF(pos.X - difference, pos.Y - difference);

                canvas.FillRectangle(finalColor, new RectangleF(finalPos, finalScale));
            }

            void DrawBox(Point topLeft, Point topRight, Point bottomLeft, Point bottomRight, Color color, float size)
            {
                canvas.DrawLine(new Pen(color, size), topLeft, topRight);
                canvas.DrawLine(new Pen(color, size), bottomRight, topRight);
                canvas.DrawLine(new Pen(color, size), bottomRight, bottomLeft);
                canvas.DrawLine(new Pen(color, size), topLeft, bottomLeft);
            }

            var lineGridWidth = 0.1f;

            // Draw a Grid
            for (int x = 0; x < Settings.canvas_width; x++)
            {
                for (int y = 0; y < Settings.canvas_height; y++)
                {
                    var pos = new PointF((x * CanvasArea.Width) / Settings.canvas_width, (y * CanvasArea.Height) / Settings.canvas_height);

                    // Draw a "Pixel" on the canvas for each real pixel on the bitmap.
                    if (bmp.GetPixel(x, y).A > 0)
                    {
                        // Create a color based on the current color
                        var color = new SolidBrush(bmp.GetPixel(x, y));

                        DrawPixel(pos, color);
                    }
                    else
                    {
                        // Create a grid color based on the current grid color
                        DrawPixel(pos, new SolidBrush(grid.GetPixel(x, y)));
                    }

                    if ((Settings.mirroring_horizontally && x < Settings.canvas_width / 2) || Settings.mirroring_horizontally == false)
                    {
                        if (Settings.DrawLineGrid)
                            canvas.DrawRectangle(new Pen((new SolidBrush(Settings.LineGridColor)), lineGridWidth), new Rectangle(new Point((int)pos.X, (int)pos.Y), new Size((int)scale.Width, (int)scale.Height)));
                    }
                }
            }

            if (ToolManager.CurrentTool == Tools.Pencil)
            {
                if (mouse_y < 0 || mouse_x < 0) return;

                var pos = new PointF((mouse_x * CanvasArea.Width) / Settings.canvas_width, (mouse_y * CanvasArea.Height) / Settings.canvas_height);

                DrawPixel(pos, new SolidBrush(currentPixelColor), 75 / Settings.canvas_width);

                var col = Blend(currentPixelColor, currentPixelColor.GetBrightness() < 0.2f ? Color.White : Color.Black, 0.5);

                canvas.DrawRectangle(new Pen(col), new Rectangle(new Point((int)pos.X, (int)pos.Y), new Size((int)scale.Width, (int)scale.Height)));
            }

            if (ToolManager.CurrentTool == Tools.Cursor)
            {
                HighLightManager.SetMinMax(HighLightManager.startHighlight, currentMousePos);

                if (HighLightManager.endHighlight != Vector2.zero)
                {
                    HighLightManager.SetMinMax(HighLightManager.startHighlight, HighLightManager.endHighlight);
                }

                var min = HighLightManager.Min;
                var max = HighLightManager.Max;

                var minModified = new Vector2((min.x * CanvasArea.Width) / Settings.canvas_width, (min.y * CanvasArea.Height) / Settings.canvas_height);
                var maxModified = new Vector2((max.x * CanvasArea.Width) / Settings.canvas_width, (max.y * CanvasArea.Height) / Settings.canvas_height);

                var topLeft = new Point((int)(minModified.x), (int)(maxModified.y + scale.Height));
                var topRight = new Point((int)(maxModified.x + scale.Width), (int)(maxModified.y + scale.Height));
                var bottomLeft = new Point((int)(minModified.x), (int)(minModified.y));
                var bottomRight = new Point((int)(maxModified.x + scale.Width), (int)(minModified.y));

                var lineWidth = 3.5f;
                var difference = 1.5f;

                var color1 = Color.HotPink;
                var color2 = Color.LightBlue;
                var color3 = Color.White;

                DrawBox(topLeft, topRight, bottomLeft, bottomRight, color1, lineWidth + difference);
                DrawBox(topLeft, topRight, bottomLeft, bottomRight, color2, lineWidth);
                DrawBox(topLeft, topRight, bottomLeft, bottomRight, color3, lineWidth - difference);

                HighLightManager.Min = min;
                HighLightManager.Max = max;
            }

            if (ToolManager.CurrentTool == Tools.Ruler)
            {
                var start = new Vector2((HighLightManager.startHighlight.x * CanvasArea.Width) / Settings.canvas_width, (HighLightManager.startHighlight.y * CanvasArea.Height) / Settings.canvas_height);
                var end = new Vector2((currentMousePos.x * CanvasArea.Width) / Settings.canvas_width, (currentMousePos.y * CanvasArea.Height) / Settings.canvas_height);

                canvas.DrawLine(new Pen(Brushes.Black, 2), new PointF(start.x + (scale.Width / 2), start.y + (scale.Height / 2)), new PointF(end.x + (scale.Width / 2), end.y + (scale.Height / 2)));
            }
        }

        /// <summary>
        /// Get a bitmap based on a width and height
        /// </summary>
        /// <param name="width">Width of the bitmap</param>
        /// <param name="height">Height of the bitmap</param>
        /// <returns>Returns a bitmap with a grid.</returns>
        public static Bitmap GetGrid(int width, int height)
        {
            // Create a bitmap
            var bmp = new Bitmap(width, height);

            int blackX = Settings.GridScale * 2;

            // Do a 2D loop to draw the grid
            for (int x = 0; x < width; x++)
            {
                int blackY = (blackX > Settings.GridScale) ? Settings.GridScale : Settings.GridScale * 2;

                // Increment the X
                if (blackX > 1)
                    blackX--;
                else
                    blackX = Settings.GridScale * 2;

                for (int y = 0; y < height; y++)
                {
                    if (blackY > 1)
                        blackY--;
                    else
                    {
                        blackY = Settings.GridScale * 2;
                    }

                    bool drawBlack = (blackY <= Settings.GridScale);

                    // Create a grid color based on the current grid color
                    bmp.SetPixel(x, y, (drawBlack == false) ? Settings.GridColorA : Settings.GridColorB);
                }
            }

            // Give back the bitmap
            return bmp;
        }

        public static void PlaceBitmapAt(Vector2 Min, Color[,] colors)
        {
            if (colors == null) return;

            var width = colors.GetLength(0);
            var height = colors.GetLength(1);

            // Generate the saved image
            for (int x = 0; x < CanvasArea.Image.Width; x++)
            {
                for (int y = 0; y < CanvasArea.Image.Height; y++)
                {
                    var bmp_X = x - Min.x;
                    var bmp_Y = y - Min.y;

                    if (bmp_X >= 0 && bmp_X < width)
                    {
                        if (bmp_Y >= 0 && bmp_Y < height)
                        {
                            var color = colors[(int)bmp_X, (int)bmp_Y];

                            if (color.A == 0) continue;

                            DrawPixelWithoutUpdate(x, y, color);
                        }
                    }
                }
            }

            UpdateCanvas();
        }

        public static void PlacePixelsBetween(Vector2 Min, Vector2 Max, Color color)
        {
            var width = (int)(Max.x - Min.x) + 1;
            var height = (int)(Max.y - Min.y) + 1;

            // Draw a pixel at every pixel between min and max
            for (int x = 0; x < CanvasArea.Image.Width; x++)
            {
                for (int y = 0; y < CanvasArea.Image.Height; y++)
                {
                    var bmp_X = x - Min.x;
                    var bmp_Y = y - Min.y;

                    if (bmp_X >= 0 && bmp_X < width)
                    {
                        if (bmp_Y >= 0 && bmp_Y < height)
                        {
                            DrawPixelWithoutUpdate(x, y, color);
                        }
                    }
                }
            }

            UpdateCanvas();
        }
    }
}