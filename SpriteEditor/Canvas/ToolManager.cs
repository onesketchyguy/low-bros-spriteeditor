﻿using System;
using System.Windows.Forms;

namespace SpriteEditor.Canvas
{
    public enum Tools
    {
        /// <summary>
        /// Just using the cursor tool.
        /// </summary>
        Cursor,

        /// <summary>
        /// Draws a line between two points.
        /// </summary>
        Line,

        /// <summary>
        /// Drawing on individual pixels.
        /// </summary>
        Pencil,

        /// <summary>
        /// Remove pixels at any given location.
        /// </summary>
        Eracer,

        /// <summary>
        /// Picks up a color.
        /// </summary>
        Sponge,

        /// <summary>
        /// Bucket fill tool. Replaces all pixels within an area with a specified color.
        /// </summary>
        Fill,

        /// <summary>
        /// Find out how much space is between two points.
        /// </summary>
        Ruler
    }

    internal class ToolManager
    {
        public static Tools CurrentTool = Tools.Pencil;

        public delegate void OnToolChangedCallback(Tools currentTool);

        public static OnToolChangedCallback ToolChangedCallback;

        public static void OnRecievedSingleKeyInput(Keys keyPressed)
        {
            switch (keyPressed)
            {
                case Keys.C:
                    // Cursor button
                    CurrentTool = Tools.Cursor;
                    break;

                case Keys.D:
                    // Draw button
                    CurrentTool = Tools.Pencil;
                    break;

                case Keys.E:
                    // Eracer button
                    CurrentTool = Tools.Eracer;
                    break;

                case Keys.F:
                    // Fill button
                    CurrentTool = Tools.Fill;
                    break;

                case Keys.S:
                    // Sponge button
                    CurrentTool = Tools.Sponge;
                    break;

                case Keys.L:
                    // Line button
                    CurrentTool = Tools.Line;
                    break;

                case Keys.R:
                    // Measure button
                    CurrentTool = Tools.Ruler;
                    break;

                default:
                    break;
            }

            if (ToolChangedCallback != null) ToolChangedCallback.Invoke(CurrentTool);

            Console.WriteLine($"Current tool switched... {CurrentTool}");
        }
    }
}