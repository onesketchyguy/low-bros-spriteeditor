﻿using System;
using System.Windows.Forms;

namespace SpriteEditor.Canvas
{
    public partial class CanvasSizer : Form
    {
        public int x = 0;
        public int y = 0;

        public CanvasSizer(int initX, string name = "Canvas Size", int initY = 0)
        {
            InitializeComponent();

            this.Text = name;

            SizeX.Text = initX.ToString();
            SizeY.Text = initY.ToString();

            if (initY <= 0)
            {
                ylabel.Visible = false;
                xlabel.Visible = false;

                SizeY.Visible = false;
            }
        }

        private void DoneButton_Click(object sender, EventArgs e)
        {
            x = int.Parse(SizeX.Text);

            if (SizeY.Visible)
            {
                y = int.Parse(SizeY.Text);
            }
            else y = x;

            Close(); // Close this window
        }

        private void SizeX_TextChanged(object sender, EventArgs e)
        {
            var text = SizeX.Text;

            var newText = "";

            char[] array = text.ToCharArray();
            for (int i = 0; i < array.Length; i++)
            {
                char item = array[i];
                if (Char.IsLetter(item))
                {
                    continue;
                }
                else
                {
                    newText += item;
                }
            }

            SizeX.Text = newText;
        }
    }
}