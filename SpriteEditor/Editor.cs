﻿using SpriteEditor.ActionHistory;
using SpriteEditor.Canvas;
using SpriteEditor.Color_Picker;
using SpriteEditor.FolderSystem;
using SpriteEditor.Preview_Window;
using SpriteEditor.Utilities;
using SpriteEditor.SpriteSheet;
using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using SpriteEditor.Updates;
using SpriteEditor.Highlight;
using SpriteEditor.Tilemap;

namespace SpriteEditor
{
    public partial class Editor : Form
    {
        private static Editor localInstance;

        private Color[] colors = new Color[] { Color.Black, Color.White };

        private Color currentColor_left
        {
            get
            {
                return colors.FirstOrDefault();
            }
            set
            {
                colors[0] = value;

                ShowColorOnMap(value, CurrentColor_Left);
            }
        }

        private Color currentColor_right
        {
            get
            {
                return colors.LastOrDefault();
            }
            set
            {
                colors[1] = value;

                ShowColorOnMap(value, CurrentColor_Right);
            }
        }

        public static Image CanvasImage { get; internal set; }

        public static Image GetCanvasImage;

        private Vector2 currentMousePos;

        private MouseEventArgs lastMouseEvent;
        private int mouse_x = -1, mouse_y = -1;

        public Editor()
        {
            InitializeComponent();

            localInstance = this;

            // Setup colors / Load settings
            new ColorPicker(ColorsRegion);
            ColorPicker.colors = ColorPicker.CreateColorsFromImage(Image.FromFile("Resources/Palettes/BaseColorPalette.png"));
            ColorPicker.Sort(SortMode.Brightness);
            ColorPicker.SetupColorPalette(true);

            ColorPicker.SetColor(ColorPicker.colors.FirstOrDefault(), MouseButtons.Left);
            ColorPicker.SetColor(ColorPicker.colors.LastOrDefault(), MouseButtons.Right);

            Prefrences.PrefrencesManager.LoadFile();
            lineGridToolStripMenuItem.Checked = Settings.DrawLineGrid;

            Reset();

            // Show the preview window
            var form = new PreviewWindow();
            form.FormClosed += (s, args) => previewWindowToolStripMenuItem.Checked = false;
            form.Show();

            previewWindowToolStripMenuItem.Checked = true;

            ToolManager.ToolChangedCallback += HandleTools;

            colorSpectrum.Image = CustomColorPicker.GetFullSpectrumBitmap(colorSpectrum.Width, colorSpectrum.Height);
            colorBrightness.Image = CustomColorPicker.GetBrightnessSpectrumBitmap(colorSpectrum.Width, colorSpectrum.Height, CurrentColor_Left.BackColor);
        }

        public void Reset()
        {
            // Create a new canvas to draw onto
            new CanvasPainter(Settings.canvas_width, Settings.canvas_height, CanvasArea);

            // Make a new sprite sheet panel
            SpriteSheetManager.Reset(spritesLayout, CanvasArea);

            // Clear the history
            History.Initialize(CanvasArea.Image);
        }

        private void HandleToolInput(MouseEventArgs e, int x, int y)
        {
            if (ToolManager.CurrentTool != Tools.Cursor)
            {
                HighLightManager.Clear();

                CanvasArea.Invalidate();
            }

            currentMousePos = new Vector2(mouse_x, mouse_y);

            lastMouseEvent = e;

            HandleTools(ToolManager.CurrentTool);
        }

        private void HandleTools(Tools activeTool)
        {
            toolStripText.Text = $"";

            switch (activeTool)
            {
                case Tools.Cursor:
                    Cursor = Cursors.Arrow;

                    CanvasArea.Invalidate();

                    break;

                case Tools.Pencil:
                    Cursor = Settings.Pen;
                    if (Settings.mirroring_horizontally)
                    {
                        if (mouse_x >= Settings.canvas_width / 2) Cursor = Cursors.No;
                    }

                    if (lastMouseEvent.Button == MouseButtons.Left)
                    {
                        CanvasPainter.DrawPixelAt(mouse_x, mouse_y, currentColor_left);
                    }
                    else
                    if (lastMouseEvent.Button == MouseButtons.Right)
                    {
                        CanvasPainter.DrawPixelAt(mouse_x, mouse_y, currentColor_right);
                    }

                    toolStripText.Text = $"X:{mouse_x}, Y:{mouse_y}";

                    break;

                case Tools.Line:
                    Cursor = Settings.Pen;
                    if (Settings.mirroring_horizontally)
                    {
                        if (mouse_x >= Settings.canvas_width / 2) Cursor = Cursors.No;
                    }

                    if (lastMouseEvent.Button == MouseButtons.Left)
                    {
                        CanvasPainter.PlacePixelsBetween(ShapeManager.start, ShapeManager.end, currentColor_left);
                    }
                    else
                    if (lastMouseEvent.Button == MouseButtons.Right)
                    {
                        CanvasPainter.PlacePixelsBetween(ShapeManager.start, ShapeManager.end, currentColor_right);
                    }

                    break;

                case Tools.Eracer:
                    Cursor = Settings.Eracer;

                    if (lastMouseEvent.Button == MouseButtons.Left)
                        CanvasPainter.ClearPixelAt(mouse_x, mouse_y);
                    break;

                case Tools.Fill:
                    Cursor = Settings.Bucket;

                    if (lastMouseEvent.Button == MouseButtons.Left)
                    {
                        CanvasPainter.FillAreaFrom(mouse_x, mouse_y, currentColor_left);
                    }
                    else
                    if (lastMouseEvent.Button == MouseButtons.Right)
                    {
                        CanvasPainter.FillAreaFrom(mouse_x, mouse_y, currentColor_right);
                    }
                    break;

                case Tools.Sponge:
                    Cursor = Settings.Sponge;

                    ColorPicker.SetColor(CanvasPainter.GetPixelAt(mouse_x, mouse_y), lastMouseEvent.Button);

                    break;

                case Tools.Ruler:
                    Cursor = Settings.Ruler;

                    var dist = HighLightManager.SetMinMax(HighLightManager.startHighlight, currentMousePos); // Get absolute value of both start and end positions.

                    toolStripText.Text = $"PositionA: {HighLightManager.startHighlight.ToString()}, PositionB: {currentMousePos.ToString()}, Distance: {dist}";

                    break;

                default:
                    break;
            }
        }

        private void ShowColorOnMap(Color color, PictureBox image)
        {
            // Show the current color on the current color region
            var currentColor = new Bitmap(CurrentColor_Left.Width, CurrentColor_Left.Height);

            for (int X = 0; X < currentColor.Width; X++)
            {
                for (int Y = 0; Y < currentColor.Height; Y++)
                {
                    currentColor.SetPixel(X, Y, color);
                }
            }

            // Show the current color on the current color region
            image.Image = currentColor;
            image.Invalidate();
        }

        public static void SetMouseColor(MouseButtons mouse, Color color)
        {
            // Set the drawing color to the retrieved color
            if (mouse == MouseButtons.Left)
            {
                localInstance.currentColor_left = color;
            }
            else
            if (mouse == MouseButtons.Right)
            {
                localInstance.currentColor_right = color;
            }
        }

        private void ColorPickerArea_Click(object sender, MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Left && e.Button != MouseButtons.Right) return;

            var imageBox = (PictureBox)sender;

            int x = e.Location.X;
            int y = e.Location.Y;

            if (x <= 0 || y <= 0 || x >= imageBox.Width || y >= imageBox.Height) return;

            // Set the selected color to a new color
            var bmp = (Bitmap)imageBox.Image;
            var color = bmp.GetPixel(x, y);

            ColorPicker.ChangeColor(color);
            ColorPicker.SetColor(color, e.Button);
        }

        #region Canvas Area

        private void CanvasArea_MouseEvent(object sender, MouseEventArgs e)
        {
            int x = (e.Location.X * Settings.canvas_width) / CanvasArea.Width;
            int y = (e.Location.Y * Settings.canvas_height) / CanvasArea.Height;

            mouse_x = x;
            mouse_y = y;

            HandleToolInput(e, x, y);
        }

        private void CanvasArea_MouseUp(object sender, MouseEventArgs e)
        {
            CanvasArea_MouseEvent(sender, e);

            var mousePos = new Vector2(mouse_x, mouse_y);

            ShapeManager.end = mousePos;

            if (e.Button == MouseButtons.Left)
            {
                if (ToolManager.CurrentTool != Tools.Cursor) HighLightManager.endHighlight = Vector2.zero;
                else
                {
                    HighLightManager.endHighlight = mousePos;
                }
            }
            else
            {
                HighLightManager.Clear();
            }

            History.AddNewAction(CanvasArea.Image); // Save the action to the action list
        }

        private void CanvasArea_MouseDown(object sender, MouseEventArgs e)
        {
            CanvasArea_MouseEvent(sender, e);

            var mousePos = new Vector2(mouse_x, mouse_y);

            ShapeManager.start = mousePos;

            if (e.Button == MouseButtons.Left)
            {
                HighLightManager.startHighlight = mousePos;

                HighLightManager.endHighlight = Vector2.zero;
            }
        }

        private void CanvasArea_Paint(object sender, PaintEventArgs e)
        {
            // Create a bitmap from the current image
            var bmp = (Bitmap)CanvasArea.Image;

            if (Settings.mirroring_horizontally) bmp = CanvasPainter.MirrorHorizontally(bmp);
            if (Settings.mirroring_vertically) bmp = CanvasPainter.MirrorVertically(bmp);

            // Retrieve the graphics portion of the current image
            Graphics canvas = e.Graphics;

            CanvasPainter.PaintCanvas(bmp, canvas, currentMousePos, mouse_y, mouse_x, currentColor_left);

            CanvasImage = CanvasArea.Image;
        }

        private void CanvasArea_MouseLeave(object sender, EventArgs e)
        {
            Cursor = Cursors.Default;

            mouse_x = -1;
            mouse_y = -1;

            CanvasArea.Invalidate();

            toolStripText.Text = $"";
        }

        private void CanvasArea_Resize(object sender, EventArgs e) => CanvasArea.Width = CanvasArea.Height; // Keep the canvas square

        #endregion Canvas Area

        #region Tool button click events

        private void EracerTool_Click(object sender, EventArgs e) => ToolManager.CurrentTool = Tools.Eracer;

        private void CursorTool_Click(object sender, EventArgs e) => ToolManager.CurrentTool = Tools.Cursor;

        private void PenTool_Click(object sender, EventArgs e) => ToolManager.CurrentTool = Tools.Pencil;

        private void FillTool_Click(object sender, EventArgs e) => ToolManager.CurrentTool = Tools.Fill;

        private void SpongeTool_Click(object sender, EventArgs e) => ToolManager.CurrentTool = Tools.Sponge;

        #endregion Tool button click events

        private void Editor_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Z)
            {
                History.LoadLastAction();

                return;
            }

            if (e.KeyCode == Keys.Y)
            {
                History.ReturnToLastAction();

                return;
            }

            if (e.KeyCode == Keys.Delete)
            {
                if (HighLightManager.Max == Vector2.one * -1 && HighLightManager.Min == Vector2.one * -1)
                {
                    return;
                }

                HighLightManager.PopulatePositions((Bitmap)CanvasArea.Image);

                // Remove the item from the position
                CanvasPainter.ClearPixels(HighLightManager.positions.ToArray());
            }

            // Cut from location
            if (e.KeyCode == Keys.X)
            {
                if (HighLightManager.Max == Vector2.one * -1 && HighLightManager.Min == Vector2.one * -1)
                {
                    return;
                }

                HighLightManager.SaveMap((Bitmap)CanvasArea.Image);

                HighLightManager.PopulatePositions((Bitmap)CanvasArea.Image);

                // Remove the item from the position
                CanvasPainter.ClearPixels(HighLightManager.positions.ToArray());
            }

            // Past at location
            if (e.KeyCode == Keys.V)
            {
                if (HighLightManager.Max == Vector2.one * -1 && HighLightManager.Min == Vector2.one * -1)
                {
                    return;
                }

                // Draw each pixel from the loaded map on the canvas
                CanvasPainter.PlaceBitmapAt(HighLightManager.Min, HighLightManager.storedColors);
            }

            if (e.KeyCode == Keys.Up || e.KeyCode == Keys.Left || e.KeyCode == Keys.Right || e.KeyCode == Keys.Down)
            {
                if (HighLightManager.Max == Vector2.one * -1 && HighLightManager.Min == Vector2.one * -1)
                {
                    return;
                }

                HighLightManager.PopulatePositions((Bitmap)CanvasArea.Image);

                HighLightManager.ShiftPixels((Bitmap)CanvasArea.Image, e.KeyCode);
                CanvasArea.Invalidate();
            }

            if (e.KeyCode == Keys.G)
            {
                LineGrid_Click(null, null);
            }

            if (e.KeyCode == Keys.Space)
            {
                SpriteSheetManager.AddFrame(CanvasArea.Image);

                return;
            }

            ToolManager.OnRecievedSingleKeyInput(e.KeyCode);
        }

        private void ExportImage_Click(object sender, EventArgs e)
        {
            // Ask where to save the file.
            string exportLocation = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);

            using (var dialog = new System.Windows.Forms.SaveFileDialog())
            {
                dialog.InitialDirectory = exportLocation;
                dialog.Filter = "PNG format (*.png)|*.png";
                dialog.ShowDialog();

                exportLocation = dialog.FileName;

                dialog.Dispose();
            }

            if (exportLocation == string.Empty) return; // User canceled.

            var Image = SpriteSheetManager.GetImage;

            // Save the image to disk
            FileManager.ExportFile(Image, exportLocation);
        }

        private void ImportSprite_Click(object sender, EventArgs e)
        {
            // Ask where the file is located.
            string importLocation = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);

            using (var dialog = new System.Windows.Forms.OpenFileDialog())
            {
                dialog.InitialDirectory = importLocation;
                dialog.Filter = "PNG format (*.png)|*.png";

                DialogResult result = dialog.ShowDialog();

                Console.WriteLine(dialog.FileName);

                importLocation = dialog.FileName;

                dialog.Dispose();
            }

            if (importLocation == string.Empty) return; // User canceled.

            // import the image

            var image = new Bitmap(Image.FromFile(importLocation));

            // Set the scale of the image
            Settings.canvas_width = image.Width;
            Settings.canvas_height = image.Height;

            var spriteLength = new Vector2(Settings.canvas_width, Settings.canvas_height);

            var sizer = new CanvasSizer(1, "How many sprites?", 1);
            sizer.FormClosing += (object s, FormClosingEventArgs ev) =>
            {
                if (sizer.x <= 0) sizer.x = 1;
                if (sizer.y <= 0) sizer.y = 1;

                spriteLength.x = sizer.x;
                spriteLength.y = sizer.y;
            };

            sizer.ShowDialog();

            // Import the sprites
            var sprites = SpriteSheetManager.LoadSprites(image, spriteLength);
            var mainSprite = sprites.FirstOrDefault();

            // Set the scale
            Settings.canvas_width = mainSprite.Width;
            Settings.canvas_height = mainSprite.Height;

            // Show the new image
            CanvasArea.Image = mainSprite;
            CanvasPainter.UpdateCanvas();

            // Clear the history
            History.Initialize(CanvasArea.Image);

            // Ask if we should import the colors from the sprite
            var confirmColorChange = new ConfirmDialogue("Import palette from image?");
            confirmColorChange.FormClosing += (object s, FormClosingEventArgs ev) =>
            {
                if (confirmColorChange.Confirmed)
                {
                    // Set the color palette
                    var colors = ColorPicker.CreateColorsFromImage(image);

                    ColorPicker.colors = colors;

                    ColorPicker.Sort(SortMode.Brightness);
                    ColorPicker.SetupColorPalette(true);
                }
            };

            confirmColorChange.ShowDialog();
        }

        private void ImportPalette_Click(object sender, EventArgs e)
        {
            // Ask where the file is located.
            string importLocation = Application.StartupPath + "\\Resources\\Palettes";

            using (var dialog = new System.Windows.Forms.OpenFileDialog())
            {
                dialog.InitialDirectory = importLocation;
                dialog.Filter = "PNG format (*.png)|*.png";

                System.Windows.Forms.DialogResult result = dialog.ShowDialog();

                Console.WriteLine(dialog.FileName);

                importLocation = dialog.FileName;

                dialog.Dispose();
            }

            if (importLocation == string.Empty) return; // User canceled

            // import the image

            var image = Image.FromFile(importLocation);

            // Set the color palette
            var colors = ColorPicker.CreateColorsFromImage(image);

            ColorPicker.colors = colors;

            ColorPicker.Sort(SortMode.Brightness);
            ColorPicker.SetupColorPalette(true);
        }

        private void newFrame_Click(object sender, EventArgs e) => SpriteSheetManager.AddFrame(CanvasArea.Image);

        private void NewSprite_Click(object sender, EventArgs e)
        {
            var sizer = new CanvasSizer(16, "Canvas Size");
            sizer.FormClosing += (object s, FormClosingEventArgs ev) =>
            {
                if (sizer.x == 0) return;

                Settings.canvas_width = sizer.x;
                Settings.canvas_height = sizer.y;

                Reset();
            };

            sizer.ShowDialog();
        }

        private void FeatureRequest_Click(object sender, EventArgs e)
        {
            // Link to the site we would like to navigate to
            var link = "https://www.lowbros.us/editor";

            // Start the weblink
            System.Diagnostics.Process.Start(link);
        }

        private void ReportBug_Click(object sender, EventArgs e)
        {
            // Link to the site we would like to navigate to
            var link = "https://www.lowbros.us/feedback";

            // Start the weblink
            System.Diagnostics.Process.Start(link);
        }

        private void CheckForUpdates_Click(object sender, EventArgs e)
        {
            var checker = new UpdateChecker();
            checker.ShowDialog();
        }

        private void PreviewWindow_Click(object sender, EventArgs e)
        {
            if (previewWindowToolStripMenuItem.Checked)
            {
                PreviewWindow.CloseWindow();

                previewWindowToolStripMenuItem.Checked = false;
                return;
            }

            previewWindowToolStripMenuItem.Checked = true;

            var form = new PreviewWindow();

            form.FormClosed += (s, args) =>
            {
                previewWindowToolStripMenuItem.Checked = false;
            };

            form.Show();
        }

        #region Sort modes

        private void SortByHue_Click(object sender, EventArgs e)
        {
            ColorPicker.Sort(SortMode.Hue);
            ColorPicker.SetupColorPalette(true);
        }

        private void SortBySaturation_Click(object sender, EventArgs e)
        {
            ColorPicker.Sort(SortMode.Saturation);
            ColorPicker.SetupColorPalette(true);
        }

        private void SortByBrightness_Click(object sender, EventArgs e)
        {
            ColorPicker.Sort(SortMode.Brightness);
            ColorPicker.SetupColorPalette(true);
        }

        public static void SetCheckedSortMode()
        {
            localInstance.byHueToolStripMenuItem.Checked = ColorPicker.sortMode == SortMode.Hue;
            localInstance.bySaturationToolStripMenuItem.Checked = ColorPicker.sortMode == SortMode.Saturation;
            localInstance.byBrightnessToolStripMenuItem.Checked = ColorPicker.sortMode == SortMode.Brightness;
        }

        #endregion Sort modes

        private void ResizeCanvasButton_Click(object sender, EventArgs e)
        {
            var sizer = new CanvasSizer(16);
            sizer.FormClosing += (object s, FormClosingEventArgs ev) =>
            {
                Settings.canvas_width = sizer.x;
                Settings.canvas_height = sizer.y;

                new CanvasPainter(Settings.canvas_width, Settings.canvas_height, CanvasArea, (Bitmap)CanvasArea.Image);
            };

            sizer.ShowDialog();
        }

        private void LineGrid_Click(object sender, EventArgs e)
        {
            // Start drawing a grid
            Settings.DrawLineGrid = !Settings.DrawLineGrid;

            lineGridToolStripMenuItem.Checked = Settings.DrawLineGrid;

            CanvasArea.Invalidate();
        }

        private void ColorsRegion_DragDrop(object sender, DragEventArgs e)
        {
            var data = e.Data.GetData("*.png", true);

            Console.WriteLine($"{data}");
        }

        private void FlipSpriteHorizontally_Click(object sender, EventArgs e)
        {
            // Get the current sprite
            var bmp = (Bitmap)CanvasArea.Image;

            // Create a flipped version of the sprite
            bmp.RotateFlip(RotateFlipType.RotateNoneFlipX);

            // Return the sprite to it's owner
            CanvasArea.Image = bmp;

            // Display the result
            CanvasArea.Invalidate();
        }

        private void FlipVertically_Click(object sender, EventArgs e)
        {
            // Get the current sprite
            var bmp = (Bitmap)CanvasArea.Image;

            // Create a flipped version of the sprite
            bmp.RotateFlip(RotateFlipType.RotateNoneFlipY);

            // Return the sprite to it's owner
            CanvasArea.Image = bmp;

            // Display the result
            CanvasArea.Invalidate();
        }

        private void FlipSpriteRight90_Click(object sender, EventArgs e)
        {
            // Get the current sprite
            var bmp = (Bitmap)CanvasArea.Image;

            // Create a flipped version of the sprite
            bmp.RotateFlip(RotateFlipType.Rotate90FlipNone);

            // Return the sprite to it's owner
            CanvasArea.Image = bmp;

            // Display the result
            CanvasArea.Invalidate();
        }

        private void FlipSpriteLeft90_Click(object sender, EventArgs e)
        {
            // Get the current sprite
            var bmp = (Bitmap)CanvasArea.Image;

            // Create a flipped version of the sprite
            bmp.RotateFlip(RotateFlipType.Rotate270FlipNone);

            // Return the sprite to it's owner
            CanvasArea.Image = bmp;

            // Display the result
            CanvasArea.Invalidate();
        }

        private void MirrorHorizontally_Click(object sender, EventArgs e)
        {
            Settings.mirroring_horizontally = !Settings.mirroring_horizontally;
        }

        private void MirrorVertically_Click(object sender, EventArgs e)
        {
            Settings.mirroring_vertically = !Settings.mirroring_vertically;
        }

        private void HelpButton_Click(object sender, EventArgs e)
        {
            // Open help page.

            var helpPage = new Help.HelpPage();

            helpPage.ShowDialog();
        }

        private void PrefrencesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var Pref = new Prefrences.PrefrencesDialog();

            Pref.Show();
        }

        private void ExitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Open a confirm dialogue before exiting
            var dialogue = new ConfirmDialogue("Exit?");

            dialogue.FormClosed += (s, args) =>
            {
                if (dialogue.Confirmed == true)
                {
                    Application.Exit();
                }
            };

            dialogue.Show();
        }

        private void Tiling_Click(object sender, EventArgs e)
        {
            // Open up tile view port

            if (tilingToolStripMenuItem.Checked)
                return;

            var form = new Tileview.TilingView(CanvasArea);

            tilingToolStripMenuItem.Checked = true;

            form.FormClosed += (s, args) =>
            {
                tilsetEditorButton.Checked = false;
            };

            form.Show();
        }

        /// <summary>
        /// Wether or not the user is about to close this particular instance.
        /// </summary>
        private static bool closing;

        private void Editor_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (closing) return;

            // Open a confirm dialogue before exiting
            var dialogue = new ConfirmDialogue("Exit?");

            e.Cancel = true;

            dialogue.FormClosed += (s, args) =>
            {
                if (dialogue.Confirmed == true)
                {
                    Application.Exit();
                    closing = true;

                    ((Editor)sender).Close();
                }
            };

            dialogue.Show();
        }

        private void OpenTilesetEditor_Click(object sender, EventArgs e)
        {
            // Open up tileset editor

            if (tilsetEditorButton.Checked)
            {
                // Close editor

                return;
            }

            tilsetEditorButton.Checked = true;

            var form = new TilesetManager(CanvasArea);

            form.FormClosed += (s, args) =>
            {
                tilsetEditorButton.Checked = false;
            };

            form.Show();
        }
    }
}