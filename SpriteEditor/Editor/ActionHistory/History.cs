﻿using SpriteEditor.Canvas;
using SpriteEditor.Utilities;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace SpriteEditor.ActionHistory
{
    public class History
    {
        private static List<Action> actions = new List<Action>();

        private static int currentIndex = 0;

        private static Image lastImage;

        private static void AddAction(Action newAction)
        {
            for (int i = actions.Count - 1; i > currentIndex; i--)
            {
                actions.RemoveAt(i);
            }

            actions.Add(newAction);
        }

        public static void Initialize(Image image)
        {
            lastImage = image;
            actions.Clear();
            actions.Add(new Action
            {
                ChangedPixels = new Pixel[]
                {
                    new Pixel(0, 0, Color.Empty)
                }
            });
        }

        public static void AddNewAction(Image image)
        {
            if (image == null) return;

            var newBmp = (Bitmap)image;
            var lastBMP = (Bitmap)lastImage;

            lastImage = new Bitmap((Bitmap)image);

            var action = new Action();

            for (int x = 0; x < image.Width; x++)
            {
                for (int y = 0; y < image.Height; y++)
                {
                    var newBit = newBmp.GetPixel(x, y);
                    var lastBit = lastBMP.GetPixel(x, y);

                    if (newBit != lastBit)
                    {
                        action.Add(new Pixel(x, y, lastBit));
                    }
                }
            }

            if (action.ChangedPixels == null) return;

            AddAction(action);
            currentIndex = actions.Count - 1;
        }

        public static void LoadLastAction()
        {
            if (actions == null) return;

            var action = actions.FirstOrDefault();

            if (actions.Count <= 1 || currentIndex <= 1)
            {
                // the only direction to go from here is by clearing the map

                CanvasPainter.ClearCanvas();
                currentIndex = 1;
            }
            else
            {
                if (actions.Count > currentIndex)
                {
                    action = actions[currentIndex];

                    currentIndex--;
                }

                if (action.ChangedPixels == null) return;

                foreach (var item in action.ChangedPixels)
                {
                    CanvasPainter.DrawPixelWithoutUpdate((int)item.position.x, (int)item.position.y, item.color);
                }
            }

            CanvasPainter.UpdateCanvas();
        }

        public static void ReturnToLastAction()
        {
            if (actions == null) return;

            var action = actions[currentIndex];

            if (currentIndex < actions.Count - 1)
                currentIndex++;

            if (action.ChangedPixels == null) return;

            foreach (var item in action.ChangedPixels)
            {
                CanvasPainter.DrawPixelWithoutUpdate((int)item.position.x, (int)item.position.y, item.color);
            }

            CanvasPainter.UpdateCanvas();
        }

        public struct Action
        {
            public Pixel[] ChangedPixels;

            public void Add(Pixel pixel)
            {
                if (ChangedPixels == null)
                {
                    ChangedPixels = new Pixel[]
                    {
                        pixel
                    };

                    return;
                }

                var pixels = new Pixel[ChangedPixels.Length + 1];

                for (int i = 0; i < ChangedPixels.Length; i++)
                {
                    pixels[i] = ChangedPixels[i];
                }

                pixels[ChangedPixels.Length] = pixel;

                ChangedPixels = pixels;
            }
        }

        public struct Pixel
        {
            public Color color;

            public Vector2 position;

            public Pixel(int x, int y, Color oldColor)
            {
                this.color = oldColor;

                position = new Vector2(x, y);
            }
        }
    }
}