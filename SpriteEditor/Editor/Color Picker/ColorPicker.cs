﻿using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SpriteEditor.Color_Picker
{
    public enum SortMode { Unsorted, Brightness, Hue, Saturation }

    public class ColorPicker
    {
        private static Color[,] plus = new Color[,]
        {
                { Color.White, Color.White, Color.Black, Color.Black, Color.White, Color.White },
                { Color.White, Color.White, Color.Black, Color.Black, Color.White, Color.White },
                { Color.Black, Color.Black, Color.Black, Color.Black, Color.Black, Color.Black },
                { Color.Black, Color.Black, Color.Black, Color.Black, Color.Black, Color.Black },
                { Color.White, Color.White, Color.Black, Color.Black, Color.White, Color.White },
                { Color.White, Color.White, Color.Black, Color.Black, Color.White, Color.White },
        };

        public static Color[] colors = new Color[]
        {
            Color.Black,
            Color.Gray,
            Color.DarkGray,
            Color.White,
            Color.PaleVioletRed,
            Color.Red,
            Color.Purple,
            Color.Blue,
            Color.Green,
            Color.Lime,
            Color.Yellow,
            Color.Orange,
        };

        public static SortMode sortMode;

        public static void Sort(SortMode mode)
        {
            sortMode = mode;

            switch (mode)
            {
                case SortMode.Unsorted:
                    break;

                case SortMode.Brightness:
                    SortByBrightness();
                    break;

                case SortMode.Hue:
                    SortByHue();
                    break;

                case SortMode.Saturation:
                    SortBySaturation();
                    break;

                default:
                    break;
            }

            Editor.SetCheckedSortMode();
        }

        private static Panel Palette;

        private static SelectedColor selectedColor;

        public ColorPicker(Panel palette)
        {
            Palette = palette;
        }

        public static void SetupColorPalette(bool resort)
        {
            Palette.Controls.Clear();

            int size = 16;

            // Create buttons for each color
            for (var index = 0; index < colors.Length; index++)
            {
                var image = new PictureBox();
                image.Size = new Size(size, size);
                image.Margin = new Padding(0, 0, 0, 0);

                var color = colors[index];
                image.BackColor = color;

                image.MouseClick += ColorClicked;

                image.Name = color.ToString();

                Palette.Controls.Add(image);
            }

            // Create an add color button
            var addColorButton = new PictureBox();
            addColorButton.Size = new Size(size, size);
            addColorButton.Margin = new Padding(0, 0, 0, 0);

            addColorButton.BackColor = Color.GhostWhite;

            // Add an image to the addColorButton
            var bmp = new Bitmap(plus.GetLength(0), plus.GetLength(1));
            for (int x = 0; x < bmp.Width; x++)
            {
                for (int y = 0; y < bmp.Height; y++)
                {
                    bmp.SetPixel(x, y, plus[x, y]);
                }
            }
            addColorButton.Image = bmp;

            addColorButton.MouseClick += NewColorClicked;

            Palette.Controls.Add(addColorButton);

            if (resort)
            {
                // Sort the colors by the stored color mode
                Sort(sortMode);
            }
        }

        private static void NewColorClicked(object sender, MouseEventArgs e)
        {
            // Add a new color
            var newColors = new Color[colors.Length + 1];
            var existingColors = colors;

            // Find the clicked color
            for (int i = 0; i < existingColors.Length; i++)
            {
                newColors[i] = existingColors[i];
            }

            newColors[existingColors.Length] = Color.Empty;

            colors = newColors;

            SetupColorPalette(false);

            SetColor(newColors[existingColors.Length], e.Button);

            //Editor.UpdateBrightnessMap();
        }

        private static void ColorClicked(object sender, MouseEventArgs e)
        {
            // Retrieve the color
            var color = Color.Empty;

            // Find the clicked color
            for (int i = 0; i < Palette.Controls.Count; i++)
            {
                var item = Palette.Controls[i];

                if (item == sender) // this is the selected color
                {
                    color = item.BackColor;
                }
            }

            SetColor(color, e.Button);
        }

        public static void SetColor(Color color, MouseButtons button)
        {
            if (button == MouseButtons.Left)
            {
                // Find the clicked color only if this is the left click
                for (int i = 0; i < Palette.Controls.Count; i++)
                {
                    var item = (PictureBox)Palette.Controls[i];

                    if (item.BackColor == color) // this is the selected color
                    {
                        item.BorderStyle = BorderStyle.Fixed3D;

                        selectedColor.index = i;

                        selectedColor.PictureBox = item;
                    }
                    else
                    {
                        item.BorderStyle = BorderStyle.None;
                    }
                }
            }

            // Set the drawing color to the retrieved color
            Editor.SetMouseColor(button, color);
        }

        public static void ChangeColor(Color newColor)
        {
            if (selectedColor.PictureBox == null) return; // Dont try to set the new color if there is no color to set.

            selectedColor.PictureBox.BackColor = newColor;

            colors[selectedColor.index] = newColor;

            selectedColor.PictureBox.Invalidate();
        }

        private static void SortByBrightness()
        {
            Task<Color[]> task = Task.Run(() =>
            {
                // Store a list for the colors.
                var sortColors = new List<Color>() { };

                for (int i = 0; i < colors.Length; i++)
                {
                    sortColors.Add(colors[i]);
                }
                sortColors.Sort((color1, color2) => (color1.GetBrightness()).CompareTo(color2.GetBrightness()));

                return sortColors.ToArray();
            });

            colors = task.Result;

            task.Dispose();
        }

        private static void SortByHue()
        {
            Task<Color[]> task = Task.Run(() =>
            {
                // Store a list for the colors.
                var sortColors = new List<Color>() { };

                for (int i = 0; i < colors.Length; i++)
                {
                    sortColors.Add(colors[i]);
                }
                sortColors.Sort((color1, color2) => (color1.GetHue()).CompareTo(color2.GetHue()));

                return sortColors.ToArray();
            });

            colors = task.Result;

            task.Dispose();
        }

        private static void SortBySaturation()
        {
            Task<Color[]> task = Task.Run(() =>
            {
                // Store a list for the colors.
                var sortColors = new List<Color>() { };

                for (int i = 0; i < colors.Length; i++)
                {
                    sortColors.Add(colors[i]);
                }
                sortColors.Sort((color1, color2) => (color1.GetSaturation()).CompareTo(color2.GetSaturation()));

                return sortColors.ToArray();
            });

            colors = task.Result;

            task.Dispose();
        }

        /// <summary>
        /// Go through every pixel in an image to import the colors.
        /// </summary>
        /// <param name="image"></param>
        /// <returns></returns>
        public static Color[] CreateColorsFromImage(Image image)
        {
            Task<Color[]> task = Task.Run(() =>
            {
                // Store a list for the colors.
                var colors = new HashSet<Color>();

                // Create a bitmap out of the image.
                var bmp = (Bitmap)image;

                for (int x = 0; x < image.Width; x++)
                {
                    for (int y = 0; y < image.Height; y++)
                    {
                        // Get a color from the selected pixel
                        var color = bmp.GetPixel(x, y);

                        // Check if the palette already has the selected color, and if not to do add it.
                        if (colors.Contains(color) == false && color.A > 0) colors.Add(color);
                    }
                }

                return colors.ToArray();
            });

            var toReturn = task.Result;

            task.Dispose();

            // Return the newly created array
            return toReturn;
        }

        private struct SelectedColor
        {
            public int index;
            public PictureBox PictureBox;
        }
    }
}