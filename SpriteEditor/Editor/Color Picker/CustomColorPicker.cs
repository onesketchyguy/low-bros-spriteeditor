﻿using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace SpriteEditor.Color_Picker
{
    public class CustomColorPicker
    {
        public static Bitmap GetFullSpectrumBitmap(int width, int height)
        {
            // Get a color map
            var colorMap = GetColorArray(width, 1);
            //var brightnessmap = GetBrightnessArray(width, height, Color.White, 3);

            var spectrumMap = new Bitmap(width, height);

            // Add weight to it.
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    var color = colorMap[x, 0];
                    //var brightness = brightnessmap[x, y];

                    var newColor = GetColorBrightness(color, 1, height, 0, y);

                    spectrumMap.SetPixel(x, y, newColor);
                }
            }

            return spectrumMap;
        }

        public static Bitmap GetBrightnessSpectrumBitmap(int width, int height, Color color)
        {
            var spectrumMap = new Bitmap(width, height);

            // Add weight to it.
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    var newColor = GetColorBrightness(color, 1, height, 0, y);

                    spectrumMap.SetPixel(x, y, newColor);
                }
            }

            return spectrumMap;
        }

        private static Color GetColorBrightness(Color colorToUse, int width, int height, int x, int y)
        {
            var colorMap = GetBrightnessArray(width, height, colorToUse, 3);

            return colorMap[x, y];
        }

        public static Color[,] GetColorArray(int width, int height)
        {
            var colorMap = new Color[width, height];

            // Draw the promary colors to the map

            var Colors = new Color[]
            {
                Color.LightPink,
                Color.Red,
                Color.Purple,
                Color.Blue,
                Color.Green,
                Color.Lime,
                Color.Yellow,
                Color.DarkOrange,
                Color.Red
            };

            // Create a gradient between the colors.

            int merges = 0;

            while (merges < 4)
            {
                Colors = GetMergedColors(Colors).ToArray();
                merges++;
            }

            // Paint the color map

            int index = 0;

            int offset = width / Colors.Length;

            for (int x = 0; x < width; x++)
            {
                if (x >= offset)
                {
                    if (index + 1 < Colors.Length)
                        index++;

                    offset += width / Colors.Length;
                }

                var color = Colors[index];

                for (int y = 0; y < height; y++)
                {
                    colorMap[x, y] = color;
                }
            }

            return colorMap;
        }

        public static Color[,] GetBrightnessArray(int width, int height, Color colorToUse, int weight)
        {
            var colorMap = new Color[width, height];

            // Draw the promary colors to the map

            var Colors = new Color[]
            {
                Color.White, colorToUse, Color.Black
            };

            // Create a gradient between the colors.

            int merges = 0;

            while (merges < weight)
            {
                Colors = GetMergedColors(Colors).ToArray();
                merges++;
            }

            // Paint the color map

            int index = 0;

            int offset = height / Colors.Length;

            for (int y = 0; y < height; y++)
            {
                if (y >= offset)
                {
                    if (index + 1 < Colors.Length)
                        index++;

                    offset += height / Colors.Length;
                }

                var color = Colors[index];

                for (int x = 0; x < width; x++)
                {
                    colorMap[x, y] = color;
                }
            }

            return colorMap;
        }

        private static HashSet<Color> GetMergedColors(Color[] Colors)
        {
            var colors = new HashSet<Color>();

            for (int i = 0; i < Colors.Length; i++)
            {
                if (i + 1 < Colors.Length)
                {
                    var colorA = Colors[i];
                    var colorB = Colors[i + 1];

                    var newColor = Canvas.CanvasPainter.Blend(colorA, colorB, 0.5);

                    colors.Add(colorA);
                    colors.Add(newColor);
                }
                else
                {
                    colors.Add(Colors[i]);
                }
            }

            return colors;
        }
    }
}