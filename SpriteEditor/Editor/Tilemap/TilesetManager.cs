﻿using SpriteEditor.FolderSystem;
using SpriteEditor.Utilities;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace SpriteEditor.Tilemap
{
    public partial class TilesetManager : Form
    {
        private List<PictureBox> Tiles = new List<PictureBox>();
        private List<Button> buttons = new List<Button>();

        private Vector2 tileSize = new Vector2(40, 40);

        private float distanceBetweenTiles = 1f;

        private PictureBox Canvas;

        public TilesetManager(PictureBox pictureBox)
        {
            InitializeComponent();

            Canvas = pictureBox;

            AddTile(new Vector2(188, 186));
        }

        private void AddTile(Vector2 pos)
        {
            // Ensure each tile is unique.
            foreach (var item in Tiles)
            {
                item.Image = new Bitmap(item.Image);
                item.BorderStyle = BorderStyle.None;
            }

            var tile = new PictureBox();
            tile.Image = Canvas.Image; // Load the tile from the sprites
            tile.BorderStyle = BorderStyle.FixedSingle;
            tile.Location = pos;
            tile.Size = tileSize;

            tile.MouseClick += OnTileClicked;

            Canvas.Invalidated += (object sender, InvalidateEventArgs e) => tile.Refresh();

            tile.Paint += DrawTile;

            // Setup buttons
            var right = pos + new Vector2(tileSize.x * distanceBetweenTiles, 0);
            var left = pos - new Vector2(tileSize.x * distanceBetweenTiles, 0);
            var down = pos + new Vector2(0, tileSize.y * distanceBetweenTiles);
            var up = pos - new Vector2(0, tileSize.y * distanceBetweenTiles);

            if (positionTaken(down) == false) AddButton(down);
            if (positionTaken(up) == false) AddButton(up);
            if (positionTaken(right) == false) AddButton(right);
            if (positionTaken(left) == false) AddButton(left);

            Tiles.Add(tile);
            groupBox.Controls.Add(tile);
        }

        private void AddButton(Vector2 pos)
        {
            var button = new Button();
            button.Location = pos;
            button.Size = tileSize;
            button.Click += Button_Click;

            buttons.Add(button);
            groupBox.Controls.Add(button);
        }

        private void OnTileClicked(object sender, MouseEventArgs e)
        {
            foreach (var item in Tiles)
            {
                item.BorderStyle = BorderStyle.None;
            }

            if (e.Button == MouseButtons.Left)
            {
                // Set as current frame
                for (int i = 0; i < Tiles.Count; i++)
                {
                    var button = Tiles[i];

                    if (button == sender)
                    {
                        Canvas.Image = button.Image;

                        button.BorderStyle = BorderStyle.FixedSingle;

                        button.Image = Canvas.Image;
                    }
                }
            }
            else
            if (e.Button == MouseButtons.Right)
            {
                // Delete frame

                if (Tiles.Count <= 1) return; // Not enough frames to delete this one. Clear current frame (TO BE IMPLEMENTED)

                for (int i = 0; i < Tiles.Count; i++)
                {
                    var button = Tiles[i];

                    if (button == sender)
                    {
                        bool delete = false;

                        var dialogue = new ConfirmDialogue($"Delete frame at index: {i}");
                        dialogue.FormClosing += (object s, FormClosingEventArgs ev) =>
                        {
                            delete = dialogue.Confirmed;
                        };

                        dialogue.ShowDialog();

                        if (delete)
                        {
                            // Create a function that will handle this... TO BE IMPLEMENTED!

                            Tiles.Remove(button);
                            groupBox.Controls.Remove(button);

                            AddButton((Vector2)button.Location);

                            break;
                        }
                        else break;
                    }
                }
            }
        }

        private bool positionTaken(Vector2 pos)
        {
            for (int i = 0; i < Tiles.Count; i++)
            {
                var tilePos = (Vector2)Tiles[i].Location;

                if (tilePos == pos)
                {
                    return true;
                }
            }

            for (int i = 0; i < buttons.Count; i++)
            {
                var buttonPos = (Vector2)buttons[i].Location;

                if (buttonPos == pos)
                {
                    return true;
                }
            }

            return false;
        }

        private void Button_Click(object sender, EventArgs e)
        {
            // Find the current button and retrieve it's current position
            var button = buttons.FirstOrDefault();

            for (int button_index = 0; button_index < buttons.Count; button_index++)
            {
                var item = buttons[button_index];

                if (item == sender)
                {
                    button = item;

                    break;
                }
            }

            // Remove the current button from the viewport
            groupBox.Controls.Remove(button);
            buttons.Remove(button);

            // Draw a new tile at the selected position.
            var pos = new Vector2(button.Location.X, button.Location.Y);

            AddTile(pos);
        }

        private void DrawTile(object sender, PaintEventArgs e)
        {
            var image = Tiles.FirstOrDefault();

            for (int i = 0; i < Tiles.Count; i++)
            {
                var index = Tiles[i];

                if (index == sender)
                {
                    image = index;
                    break;
                }
            }

            // Create a bitmap from the current image
            var bmp = (Bitmap)image.Image;

            // Retrieve the graphics portion of the current image
            Graphics canvas = e.Graphics;

            // Whipe the canvas clean
            canvas.Clear(Color.LightGray);

            var filling = 0.1f;

            // Create a scale and add the difference for the gaps
            var scale = new SizeF((image.Width / Settings.canvas_width) + filling, (image.Height / Settings.canvas_height) + filling);

            void DrawPixel(PointF pos, SolidBrush color)
            {
                canvas.FillRectangle(color, new RectangleF(pos, scale));
            }

            bool gray = false;
            // Draw a Grid
            for (int x = 0; x < Settings.canvas_width; x++)
            {
                gray = !gray;

                for (int y = 0; y < Settings.canvas_height; y++)
                {
                    var pos = new PointF((x * image.Width) / Settings.canvas_width, (y * image.Height) / Settings.canvas_height);

                    // Create a color based on the current color
                    var gridColor = new SolidBrush(Color.LightBlue);

                    if (gray == false)
                    {
                        gridColor = new SolidBrush(Color.LightPink);
                    }

                    gray = !gray;

                    // Draw a "Pixel" on the canvas for each real pixel on the bitmap.

                    if (bmp.GetPixel(x, y).A > 0)
                    {
                        // Create a color based on the current color
                        var color = new SolidBrush(bmp.GetPixel(x, y));

                        DrawPixel(pos, color);
                    }
                    else
                    {
                        // Create a color based on the current color
                        DrawPixel(pos, gridColor);
                    }
                }
            }
        }

        private void ExportTilset_Click(object sender, EventArgs e)
        {
            // Ask where to save the file.
            string exportLocation = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);

            using (var dialog = new System.Windows.Forms.SaveFileDialog())
            {
                dialog.InitialDirectory = exportLocation;
                dialog.Filter = "PNG format (*.png)|*.png";
                dialog.ShowDialog();

                exportLocation = dialog.FileName;

                dialog.Dispose();
            }

            if (exportLocation == string.Empty) return; // User canceled.

            var Image = GetImage();

            // Save the image to disk
            FileManager.ExportFile(Image, exportLocation);
        }

        private Bitmap GetImage()
        {
            // Grab each tile
            var tileArray = Tiles.ToArray();

            var size = GetScale();

            // Draw the sheet to a bmp
            var image = new Bitmap((int)size.x, (int)size.y);

            var offsetX = 0;
            var offsetY = 0;

            // Go through each tile and try to draw them to the total image
            for (int i = 0; i < tileArray.Length; i++)
            {
                PictureBox item = tileArray[i];
                var bmp = (Bitmap)item.Image;

                // Draw the selected tile to the image
                for (int x = 0; x < bmp.Width; x++)
                {
                    for (int y = 0; y < bmp.Height; y++)
                    {
                        var color = bmp.GetPixel(x, y); // TryGet pixel from location

                        if (color.A > 0) // Pixel located here
                            image.SetPixel(x + offsetX, y + offsetY, color); // Draw the pixel to the image
                    }
                }

                // Move the offset for the next image
                if (offsetX + bmp.Width >= image.Width - (bmp.Width / 4))
                {
                    offsetX = 0;
                    offsetY += bmp.Height;
                }
                else
                {
                    offsetX += bmp.Width;
                }
            }

            // After creating the image return it to the messenger
            return image;
        }

        private Vector2 GetScale()
        {
            var maxBmpSize = new Vector2();

            var maxPos = new Vector2();

            Tiles.Sort((item1, item2) => (item1.Location.Y.CompareTo(item2.Location.Y)));

            Console.WriteLine(Tiles[0].Location.Y);

            for (int i = 0; i < Tiles.Count; i++)
            {
                PictureBox item = Tiles[i];

                if (item.Location.Y > maxPos.y)
                {
                    maxPos.y = item.Location.Y;
                    maxBmpSize.y += item.Image.Height;
                }
            }

            Tiles.Sort((item1, item2) => (item1.Location.X.CompareTo(item2.Location.X)));

            Console.WriteLine(Tiles[0].Location.X);

            for (int i = 0; i < Tiles.Count; i++)
            {
                PictureBox item = Tiles[i];

                if (item.Location.X > maxPos.x)
                {
                    maxPos.x = item.Location.X;
                    maxBmpSize.x += item.Image.Width;
                }
            }

            return maxBmpSize;
        }
    }
}