﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using SpriteEditor.Utilities;

namespace SpriteEditor.SpriteSheet
{
    public class SpriteSheetManager
    {
        private static readonly Size size = new Size(20, 20);

        public static PictureBox displayImage;

        private static FlowLayoutPanel sheetPanel;

        public static Image GetImage
        {
            get
            {
                // Set the size of the image
                var width = 0;
                var height = 0;

                foreach (var item in frames)
                {
                    var bmp = (Bitmap)item.Image;

                    height = bmp.Height;
                    width += bmp.Width;
                }

                // Create the image
                var image = new Bitmap(width, height);

                var offset = 0;

                foreach (var item in frames)
                {
                    var bmp = (Bitmap)item.Image;

                    for (int x = 0; x < bmp.Width; x++)
                    {
                        for (int y = 0; y < bmp.Height; y++)
                        {
                            var color = bmp.GetPixel(x, y);

                            if (color.A > 0) // Pixel located here
                            {
                                image.SetPixel(x + offset, y, color);
                            }
                        }
                    }

                    offset += bmp.Width;
                }

                return image;
            }
        }

        internal static List<Image> GetFrames()
        {
            var fr = new List<Image>();

            foreach (var item in frames)
            {
                fr.Add(item.Image);
            }

            return fr;
        }

        private static List<PictureBox> frames = new List<PictureBox>();

        public static void SetFirstFrame(FlowLayoutPanel layoutPanel, Image sprite)
        {
            var newFrame = new PictureBox();
            newFrame.Name = $"{frames.Count}";
            newFrame.Size = size;
            newFrame.Image = (Bitmap)sprite;

            newFrame.BorderStyle = BorderStyle.Fixed3D;
            newFrame.MouseClick += NewFrame_MouseDown;

            displayImage.Invalidated += (object sender, InvalidateEventArgs e) => newFrame.Invalidate();

            frames.Add(newFrame);
            layoutPanel.Controls.Add(newFrame);

            sheetPanel = layoutPanel;
        }

        public static void AddFrame(Image sprite)
        {
            // Check our current frame.
            var current = frames.FirstOrDefault();

            for (int i = 0; i < frames.Count; i++)
            {
                var frame = frames[i];

                if (frame.Image == displayImage.Image)
                {
                    current = frame;
                }
            }

            var currentIndex = int.Parse(current.Name);

            // Move all frames above current frame up
            for (int i = 0; i < frames.Count; i++)
            {
                var frame = frames[i];
                var index = int.Parse(frame.Name);

                if (currentIndex < index)
                {
                    frame.Name = $"{index + 1}";
                }
            }

            // Ensure all images are unique images
            foreach (var item in frames)
            {
                item.Image = new Bitmap((Bitmap)item.Image);
                item.BorderStyle = BorderStyle.FixedSingle;
            }

            // Add a frame next to our current one.

            var newFrame = new PictureBox();
            newFrame.Name = $"{currentIndex + 1}";
            newFrame.Size = size;
            newFrame.Image = (Bitmap)sprite;

            newFrame.BorderStyle = BorderStyle.Fixed3D;
            newFrame.MouseDown += NewFrame_MouseDown;

            displayImage.Invalidated += (object sender, InvalidateEventArgs e) => newFrame.Invalidate();

            frames.Add(newFrame);
            sheetPanel.Controls.Add(newFrame);

            // Sort the frames.
            frames.Sort((object1, object2) => (int.Parse(object1.Name)).CompareTo(int.Parse(object2.Name)));

            for (int i = 0; i < frames.Count; i++) sheetPanel.Controls.SetChildIndex(frames[i], i);
        }

        public static void RemoveFrame(PictureBox toRemove)
        {
            // Check our current frame.
            var current = frames.FirstOrDefault();

            for (int i = 0; i < frames.Count; i++)
            {
                var frame = frames[i];

                if (frame.Image == toRemove.Image)
                {
                    current = frame;
                }
            }

            var currentIndex = int.Parse(current.Name);

            // Move all frames above current frame down
            for (int i = 0; i < frames.Count; i++)
            {
                var frame = frames[i];
                var index = int.Parse(frame.Name);

                if (currentIndex < index)
                {
                    frame.Name = $"{index - 1}";
                }
            }

            // Remove the frame from our lists

            frames.Remove(current);
            sheetPanel.Controls.RemoveAt(currentIndex);

            // Sort the frames.
            frames.Sort((object1, object2) => (int.Parse(object1.Name)).CompareTo(int.Parse(object2.Name)));

            for (int i = 0; i < frames.Count; i++) sheetPanel.Controls.SetChildIndex(frames[i], i);
        }

        /// <summary>
        /// Swap two frames
        /// </summary>
        /// <param name="frameA"></param>
        /// <param name="frameB"></param>
        public static void SwapFrames(PictureBox frameA, PictureBox frameB)
        {
            // Check our current frames.
            var left = frames.FirstOrDefault();
            var right = frames.FirstOrDefault();

            for (int i = 0; i < frames.Count; i++)
            {
                var frame = frames[i];

                if (frame.Image == frameA.Image) left = frame;
                if (frame.Image == frameB.Image) right = frame;
            }

            var LeftIndex = int.Parse(left.Name);
            var RightIndex = int.Parse(right.Name);

            frames[LeftIndex].Name = $"{RightIndex}";
            frames[RightIndex].Name = $"{LeftIndex}";

            // Sort the frames.
            frames.Sort((object1, object2) => (int.Parse(object1.Name)).CompareTo(int.Parse(object2.Name)));

            for (int i = 0; i < frames.Count; i++) sheetPanel.Controls.SetChildIndex(frames[i], i);
        }

        private static void NewFrame_MouseDown(object sender, MouseEventArgs e)
        {
            foreach (var item in frames)
            {
                item.BorderStyle = BorderStyle.FixedSingle;
            }

            if (e.Button == MouseButtons.Left)
            {
                // Set as current frame
                for (int i = 0; i < frames.Count; i++)
                {
                    var button = frames[i];

                    if (button == sender)
                    {
                        displayImage.Image = button.Image;

                        button.BorderStyle = BorderStyle.Fixed3D;

                        button.Image = displayImage.Image;
                    }
                }
            }
            else
            if (e.Button == MouseButtons.Right)
            {
                // Delete frame

                if (frames.Count <= 1) return; // Not enough frames to delete this one. Clear current frame (TO BE IMPLEMENTED)

                for (int i = 0; i < frames.Count; i++)
                {
                    var button = frames[i];

                    if (button == sender)
                    {
                        bool delete = false;

                        var dialogue = new ConfirmDialogue($"Delete frame at index: {i}");
                        dialogue.FormClosing += (object s, FormClosingEventArgs ev) =>
                        {
                            delete = dialogue.Confirmed;
                        };

                        dialogue.ShowDialog();

                        if (delete)
                        {
                            RemoveFrame(displayImage);

                            break;
                        }
                        else break;
                    }
                }
            }
        }

        public static void Reset(FlowLayoutPanel layoutPanel, PictureBox pictureBox)
        {
            if (frames != null)
                frames.Clear();
            else
            {
                frames = new List<PictureBox>();
            }

            displayImage = pictureBox;

            sheetPanel = layoutPanel;

            layoutPanel.Controls.Clear();

            SetFirstFrame(layoutPanel, pictureBox.Image);
        }

        public static void Reset(FlowLayoutPanel layoutPanel, Image sprite)
        {
            if (frames != null)
                frames.Clear();
            else
            {
                frames = new List<PictureBox>();
            }

            sheetPanel = layoutPanel;

            layoutPanel.Controls.Clear();

            SetFirstFrame(layoutPanel, sprite);
        }

        public static Bitmap[] GetSpritesFromSheet(Bitmap fromImage, Vector2 frameCount)
        {
            HashSet<Bitmap> items = new HashSet<Bitmap>();

            byte iterations = 0;

            var width = (int)(fromImage.Width / frameCount.x);
            var height = (int)(fromImage.Height / frameCount.y);

            // Import images
            while (items.Count == 0)
            {
                if (iterations > 10)
                {
                    Console.WriteLine("Unable to import images.");
                    break;
                }

                var offset_x = 0;
                var offset_y = 0;

                while (true)
                {
                    // Create an image from the offset and size
                    var bmp = new Bitmap(width, height);

                    for (int x = 0 + offset_x; x < width + offset_x; x++)
                    {
                        for (int y = 0 + offset_y; y < height + offset_y; y++)
                        {
                            if (x >= fromImage.Width || y >= fromImage.Height) break;

                            // Draw the pixels in this area to the bitmap
                            var color = fromImage.GetPixel(x, y);

                            bmp.SetPixel(x, y, color);
                        }
                    }

                    offset_x += width;

                    if (offset_x + 1 >= fromImage.Width)
                    {
                        offset_y = height;
                        offset_x = 0;
                    }

                    // Add the image to the items list
                    items.Add(bmp);
                    break;
                }

                iterations++;
            }

            return items.ToArray();
        }

        public static Bitmap[] LoadSprites(Bitmap fromImage, Vector2 spriteLength)
        {
            var sprites = GetSpritesFromSheet(fromImage, spriteLength);
            Reset(sheetPanel, sprites.FirstOrDefault());

            foreach (var sprite in sprites)
            {
                AddFrame(sprite);
            }

            return sprites;
        }
    }
}