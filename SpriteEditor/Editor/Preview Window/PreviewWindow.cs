﻿using SpriteEditor.Utilities;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace SpriteEditor.Preview_Window
{
    public partial class PreviewWindow : Form
    {
        private bool playing;

        public static bool closeWindow;

        private static Bitmap GridMap;

        private static byte timerValue = 0;

        private Image[] images;

        public PreviewWindow()
        {
            InitializeComponent();

            playing = true;
            closeWindow = false;

            // Start a timer
            updateTimer.Interval = 10;

            updateTimer.Start();

            // Update the grid when the preferences change
            Prefrences.PrefrencesManager.PreferencesChanged += () =>
            {
                GridMap = Canvas.CanvasPainter.GetGrid(Settings.canvas_width, Settings.canvas_height);
            };

            GridMap = Canvas.CanvasPainter.GetGrid(Settings.canvas_width, Settings.canvas_height);
        }

        private int index;

        private void UpdateTimer_Tick(object sender, EventArgs e)
        {
            if (closeWindow)
            {
                Close();
                return;
            }

            timerValue--;

            if (timerValue <= 0)
            {
                // Create an array of all the available images
                images = SpriteSheet.SpriteSheetManager.GetFrames().ToArray();

                if (playing)
                    index++;

                if (images.Length <= 1 || index > images.Length - 1)
                {
                    index = 0;
                }

                timerValue = (byte)(101 - SpeedSlider.Value);

                GridMap = Canvas.CanvasPainter.GetGrid(Settings.canvas_width, Settings.canvas_height);
            }

            if (images == null)
            {
                images = SpriteSheet.SpriteSheetManager.GetFrames().ToArray();
                return;
            }

            // Find the current index
            var image = images[index];

            if (image == null)
            {
                // The frame cannot be rendered, don't try nigga
                return;
            }

            // Change the current frame we are displaying
            preview.Image = new Bitmap(image);
        }

        private void PlayButton_Click(object sender, EventArgs e) => playing = !playing;

        private void Preview_Paint(object sender, PaintEventArgs e)
        {
            if (preview.Image == null) return;

            // Create a bitmap from the current image
            var bmp = (Bitmap)preview.Image;

            // Retrieve the graphics portion of the current image
            Graphics canvas = e.Graphics;
            // Whipe the canvas clean
            canvas.Clear(Color.LightGray);

            // Create a scale and add the difference for the caps
            var scale = new SizeF((preview.Width / Settings.canvas_width) + 0.1f, (preview.Height / Settings.canvas_height) + 0.1f);

            // Draw a Grid
            for (int x = 0; x < bmp.Width; x++)
            {
                for (int y = 0; y < bmp.Height; y++)
                {
                    var pos = new PointF((x * preview.Width) / Settings.canvas_width, (y * preview.Height) / Settings.canvas_height);

                    // Draw a "Pixel" on the canvas for each real pixel on the bitmap.
                    if (bmp.GetPixel(x, y).A > 0)
                    {
                        // Create a color based on the curren color
                        var color = new SolidBrush(bmp.GetPixel(x, y));

                        canvas.FillRectangle(color, new RectangleF(pos, scale));
                    }
                    else
                    {
                        if (GridMap.Width <= x || GridMap.Height <= y) continue;

                        canvas.FillRectangle(new SolidBrush(GridMap.GetPixel(x, y)), new RectangleF(pos, scale));
                    }
                }
            }
        }

        private void PreviewWindow_SizeChanged(object sender, EventArgs e)
        {
            Size = new Size(Size.Width, Size.Width);
        }

        public static void CloseWindow()
        {
            closeWindow = true;
        }
    }
}