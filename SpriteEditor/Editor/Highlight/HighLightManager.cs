﻿using SpriteEditor.Utilities;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace SpriteEditor.Highlight
{
    public class HighLightManager
    {
        public static HashSet<Vector2> positions = new HashSet<Vector2>();

        public static Vector2 startHighlight;
        public static Vector2 endHighlight;

        public static void PopulatePositions(Bitmap image)
        {
            positions = new HashSet<Vector2>();

            var width = (int)(Max.x - Min.x);
            var height = (int)(Max.y - Min.y);

            positions.Add(Min);

            // Remove the item from the position
            for (int x = 0; x < image.Width; x++)
            {
                for (int y = 0; y < image.Height; y++)
                {
                    var bmp_X = x - Min.x;
                    var bmp_Y = y - Min.y;

                    if (bmp_X >= 0 && bmp_X <= width)
                    {
                        if (bmp_Y >= 0 && bmp_Y <= height)
                        {
                            positions.Add(new Vector2(x, y));
                        }
                    }
                }
            }
        }

        public static Vector2 Max;
        public static Vector2 Min;

        public static Color[,] storedColors;

        public static bool HighlightExists => storedColors != null;

        /// <summary>
        /// Sets the min and max values, and returns the distance between them.
        /// </summary>
        /// <param name="lastPos"></param>
        /// <param name="currentPos"></param>
        /// <returns>The absolute value between the min and max</returns>
        public static float SetMinMax(Vector2 lastPos, Vector2 currentPos)
        {
            var xMin = (lastPos.x < currentPos.x) ? (int)lastPos.x : (int)currentPos.x;
            var yMin = (lastPos.y < currentPos.y) ? (int)lastPos.y : (int)currentPos.y;

            Min = new Vector2(xMin, yMin);

            var xMax = (lastPos.x > currentPos.x) ? (int)lastPos.x : (int)currentPos.x;
            var yMax = (lastPos.y > currentPos.y) ? (int)lastPos.y : (int)currentPos.y;

            Max = new Vector2(xMax, yMax);

            return Max.abs - Min.abs;
        }

        public static void Clear()
        {
            positions = new HashSet<Vector2>();
            //storedColors = null;
            Max = Vector2.one * -1;
            Min = Vector2.one * -1;
        }

        public static void SaveMap(Bitmap image)
        {
            if (image == null) return;

            // Create an area to work within
            var width = (int)(Max.x - Min.x) + 1;
            var height = (int)(Max.y - Min.y) + 1;

            if (width == 0 || height == 0) return;

            var map = new Color[width, height];

            // Generate the saved image
            for (int x = 0; x < image.Width; x++)
            {
                for (int y = 0; y < image.Height; y++)
                {
                    var bmp_X = x - (int)Min.x;
                    var bmp_Y = y - (int)Min.y;

                    if (bmp_X >= 0 && bmp_X < width)
                    {
                        if (bmp_Y >= 0 && bmp_Y < height)
                        {
                            var color = image.GetPixel(x, y);
                            map[bmp_X, bmp_Y] = color;
                        }
                    }
                }
            }

            // Save the image to the stored image file
            storedColors = map;
        }

        public static void ShiftPixels(Bitmap image, Keys keyCode)
        {
            int horizontal = keyCode == Keys.Left ? -1 : keyCode == Keys.Right ? 1 : 0;
            int vertical = keyCode == Keys.Up ? -1 : keyCode == Keys.Down ? 1 : 0;

            var newPos = new Vector2(horizontal, vertical);

            startHighlight += newPos;
            endHighlight += newPos;

            var newPositions = positions.ToArray();

            positions.Clear();

            var colorArray = new Color[newPositions.Length];

            // Get all the colors we need to move and remove them
            for (int i = 0; i < newPositions.Length; i++)
            {
                if ((int)newPositions[i].x < image.Width && (int)newPositions[i].x >= 0 && (int)newPositions[i].y < image.Height && (int)newPositions[i].y >= 0)
                {
                    var color = image.GetPixel((int)newPositions[i].x, (int)newPositions[i].y); // Get the color from the selected pixel
                    colorArray[i] = color; // Save that color

                    image.SetPixel((int)newPositions[i].x, (int)newPositions[i].y, Color.Transparent); // Remove the pixel from it's current position
                }

                newPositions[i] += newPos; // Move the position

                positions.Add(newPositions[i]); // Save the position for later
            }

            var updatedPositions = positions.ToArray();

            // Place all the moved pixels
            for (int i = 0; i < updatedPositions.Length; i++)
            {
                var color = colorArray[i];

                if (color.A > 0)
                {
                    if ((int)updatedPositions[i].x >= image.Width || (int)updatedPositions[i].x < 0 || (int)updatedPositions[i].y >= image.Height || (int)updatedPositions[i].y < 0) continue;

                    image.SetPixel((int)updatedPositions[i].x, (int)updatedPositions[i].y, color);
                }
            }
        }
    }
}