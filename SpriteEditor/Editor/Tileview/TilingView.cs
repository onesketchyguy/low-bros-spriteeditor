﻿using SpriteEditor.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SpriteEditor.Tileview
{
    public partial class TilingView : Form
    {
        private PictureBox[] images;

        public PictureBox mainImage;

        private Bitmap GridMap;

        public TilingView(PictureBox mainImage)
        {
            InitializeComponent();

            // Prepair the grid
            Prefrences.PrefrencesManager.PreferencesChanged += () =>
            {
                GridMap = Canvas.CanvasPainter.GetGrid(Settings.canvas_width, Settings.canvas_height);
            };

            GridMap = Canvas.CanvasPainter.GetGrid(Settings.canvas_width, Settings.canvas_height);

            // Set the main for the images
            this.mainImage = mainImage;

            // Populate the images array
            var objects = layoutPanel.Controls;
            images = new PictureBox[objects.Count];

            for (int i = 0; i < objects.Count; i++)
            {
                var obj = (PictureBox)objects[i];
                images[i] = obj;
                obj.Paint += Preview_Paint;
                // Update the images
                mainImage.Invalidated += (object sender, InvalidateEventArgs e) => obj.Invalidate();
            }
        }

        private void Preview_Paint(object sender, PaintEventArgs e)
        {
            var thisImage = ((PictureBox)sender);

            if (mainImage.Image == null) return;

            // Create a bitmap from the current image
            var bmp = (Bitmap)mainImage.Image;

            // Retrieve the graphics portion of the current image
            Graphics canvas = e.Graphics;
            // Whipe the canvas clean
            canvas.Clear(Color.LightGray);

            // Create a scale and add the difference for the caps
            var scale = new SizeF((thisImage.Width / Settings.canvas_width) + 0.1f, (thisImage.Height / Settings.canvas_height) + 0.1f);

            // Draw a Grid
            for (int x = 0; x < bmp.Width; x++)
            {
                for (int y = 0; y < bmp.Height; y++)
                {
                    var pos = new PointF((x * thisImage.Width) / Settings.canvas_width, (y * thisImage.Height) / Settings.canvas_height);

                    // Draw a "Pixel" on the canvas for each real pixel on the bitmap.
                    if (bmp.GetPixel(x, y).A > 0)
                    {
                        // Create a color based on the curren color
                        var color = new SolidBrush(bmp.GetPixel(x, y));

                        canvas.FillRectangle(color, new RectangleF(pos, scale));
                    }
                    else
                    {
                        if (GridMap.Width <= x || GridMap.Height <= y) continue;

                        canvas.FillRectangle(new SolidBrush(GridMap.GetPixel(x, y)), new RectangleF(pos, scale));
                    }
                }
            }
        }
    }
}