﻿using SpriteEditor.Canvas;
using System.Windows.Forms;

namespace SpriteEditor
{
    partial class Editor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Editor));
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.spriteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.paletteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.addNewFrameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resizeCanvasButton = new System.Windows.Forms.ToolStripMenuItem();
            this.flipSpriteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.horizontallyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.verticallyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.flipRight90 = new System.Windows.Forms.ToolStripMenuItem();
            this.flipLeft90 = new System.Windows.Forms.ToolStripMenuItem();
            this.prefrencesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cursorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.penToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.penToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.fillToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.spongeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eracerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mirrorSpriteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.horizontallyToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.verticallyToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.viewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.previewWindowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tilingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tilsetEditorButton = new System.Windows.Forms.ToolStripMenuItem();
            this.lineGridToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sortPaletteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.byHueToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bySaturationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.byBrightnessToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.otherToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reportBugToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.featureRequestToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.checkForUpdatesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.spritesLayout = new System.Windows.Forms.FlowLayoutPanel();
            this.toolsPanelLayout = new System.Windows.Forms.FlowLayoutPanel();
            this.drawButton = new System.Windows.Forms.PictureBox();
            this.FillBucket = new System.Windows.Forms.PictureBox();
            this.Eracer = new System.Windows.Forms.PictureBox();
            this.Sponge = new System.Windows.Forms.PictureBox();
            this.colorRegionLayout = new System.Windows.Forms.FlowLayoutPanel();
            this.ColorsRegion = new System.Windows.Forms.FlowLayoutPanel();
            this.CurrentColor_Left = new System.Windows.Forms.PictureBox();
            this.CurrentColor_Right = new System.Windows.Forms.PictureBox();
            this.colorSpectrum = new System.Windows.Forms.PictureBox();
            this.toolTipStrip = new System.Windows.Forms.ToolStrip();
            this.NewFrameButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripText = new System.Windows.Forms.ToolStripLabel();
            this.CanvasArea = new System.Windows.Forms.PictureBox();
            this.colorBrightness = new System.Windows.Forms.PictureBox();
            this.menuStrip.SuspendLayout();
            this.toolsPanelLayout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.drawButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FillBucket)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Eracer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Sponge)).BeginInit();
            this.colorRegionLayout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CurrentColor_Left)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CurrentColor_Right)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorSpectrum)).BeginInit();
            this.toolTipStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CanvasArea)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorBrightness)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip
            // 
            this.menuStrip.AllowDrop = true;
            this.menuStrip.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.editToolStripMenuItem,
            this.editToolStripMenuItem1,
            this.toolsToolStripMenuItem,
            this.viewToolStripMenuItem,
            this.otherToolStripMenuItem,
            this.helpToolStripMenuItem});
            resources.ApplyResources(this.menuStrip, "menuStrip");
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.ShowItemToolTips = true;
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.exportToolStripMenuItem,
            this.importToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            resources.ApplyResources(this.editToolStripMenuItem, "editToolStripMenuItem");
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            resources.ApplyResources(this.newToolStripMenuItem, "newToolStripMenuItem");
            this.newToolStripMenuItem.Click += new System.EventHandler(this.NewSprite_Click);
            // 
            // exportToolStripMenuItem
            // 
            this.exportToolStripMenuItem.Name = "exportToolStripMenuItem";
            resources.ApplyResources(this.exportToolStripMenuItem, "exportToolStripMenuItem");
            this.exportToolStripMenuItem.Click += new System.EventHandler(this.ExportImage_Click);
            // 
            // importToolStripMenuItem
            // 
            this.importToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.spriteToolStripMenuItem,
            this.paletteToolStripMenuItem});
            this.importToolStripMenuItem.Name = "importToolStripMenuItem";
            resources.ApplyResources(this.importToolStripMenuItem, "importToolStripMenuItem");
            // 
            // spriteToolStripMenuItem
            // 
            this.spriteToolStripMenuItem.Name = "spriteToolStripMenuItem";
            resources.ApplyResources(this.spriteToolStripMenuItem, "spriteToolStripMenuItem");
            this.spriteToolStripMenuItem.Click += new System.EventHandler(this.ImportSprite_Click);
            // 
            // paletteToolStripMenuItem
            // 
            this.paletteToolStripMenuItem.Name = "paletteToolStripMenuItem";
            resources.ApplyResources(this.paletteToolStripMenuItem, "paletteToolStripMenuItem");
            this.paletteToolStripMenuItem.Click += new System.EventHandler(this.ImportPalette_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            resources.ApplyResources(this.exitToolStripMenuItem, "exitToolStripMenuItem");
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.ExitToolStripMenuItem_Click);
            // 
            // editToolStripMenuItem1
            // 
            this.editToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addNewFrameToolStripMenuItem,
            this.resizeCanvasButton,
            this.flipSpriteToolStripMenuItem,
            this.prefrencesToolStripMenuItem});
            this.editToolStripMenuItem1.Name = "editToolStripMenuItem1";
            resources.ApplyResources(this.editToolStripMenuItem1, "editToolStripMenuItem1");
            // 
            // addNewFrameToolStripMenuItem
            // 
            resources.ApplyResources(this.addNewFrameToolStripMenuItem, "addNewFrameToolStripMenuItem");
            this.addNewFrameToolStripMenuItem.Name = "addNewFrameToolStripMenuItem";
            this.addNewFrameToolStripMenuItem.Click += new System.EventHandler(this.newFrame_Click);
            // 
            // resizeCanvasButton
            // 
            this.resizeCanvasButton.Name = "resizeCanvasButton";
            resources.ApplyResources(this.resizeCanvasButton, "resizeCanvasButton");
            this.resizeCanvasButton.Click += new System.EventHandler(this.ResizeCanvasButton_Click);
            // 
            // flipSpriteToolStripMenuItem
            // 
            this.flipSpriteToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.horizontallyToolStripMenuItem,
            this.verticallyToolStripMenuItem,
            this.flipRight90,
            this.flipLeft90});
            this.flipSpriteToolStripMenuItem.Name = "flipSpriteToolStripMenuItem";
            resources.ApplyResources(this.flipSpriteToolStripMenuItem, "flipSpriteToolStripMenuItem");
            // 
            // horizontallyToolStripMenuItem
            // 
            this.horizontallyToolStripMenuItem.Name = "horizontallyToolStripMenuItem";
            resources.ApplyResources(this.horizontallyToolStripMenuItem, "horizontallyToolStripMenuItem");
            this.horizontallyToolStripMenuItem.Click += new System.EventHandler(this.FlipSpriteHorizontally_Click);
            // 
            // verticallyToolStripMenuItem
            // 
            this.verticallyToolStripMenuItem.Name = "verticallyToolStripMenuItem";
            resources.ApplyResources(this.verticallyToolStripMenuItem, "verticallyToolStripMenuItem");
            this.verticallyToolStripMenuItem.Click += new System.EventHandler(this.FlipVertically_Click);
            // 
            // flipRight90
            // 
            this.flipRight90.Name = "flipRight90";
            resources.ApplyResources(this.flipRight90, "flipRight90");
            this.flipRight90.Click += new System.EventHandler(this.FlipSpriteRight90_Click);
            // 
            // flipLeft90
            // 
            this.flipLeft90.Name = "flipLeft90";
            resources.ApplyResources(this.flipLeft90, "flipLeft90");
            this.flipLeft90.Click += new System.EventHandler(this.FlipSpriteLeft90_Click);
            // 
            // prefrencesToolStripMenuItem
            // 
            this.prefrencesToolStripMenuItem.Name = "prefrencesToolStripMenuItem";
            resources.ApplyResources(this.prefrencesToolStripMenuItem, "prefrencesToolStripMenuItem");
            this.prefrencesToolStripMenuItem.Click += new System.EventHandler(this.PrefrencesToolStripMenuItem_Click);
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cursorToolStripMenuItem,
            this.penToolStripMenuItem,
            this.spongeToolStripMenuItem,
            this.eracerToolStripMenuItem,
            this.mirrorSpriteToolStripMenuItem});
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            resources.ApplyResources(this.toolsToolStripMenuItem, "toolsToolStripMenuItem");
            // 
            // cursorToolStripMenuItem
            // 
            this.cursorToolStripMenuItem.Name = "cursorToolStripMenuItem";
            resources.ApplyResources(this.cursorToolStripMenuItem, "cursorToolStripMenuItem");
            this.cursorToolStripMenuItem.Click += new System.EventHandler(this.CursorTool_Click);
            // 
            // penToolStripMenuItem
            // 
            this.penToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.penToolStripMenuItem1,
            this.fillToolStripMenuItem});
            resources.ApplyResources(this.penToolStripMenuItem, "penToolStripMenuItem");
            this.penToolStripMenuItem.Name = "penToolStripMenuItem";
            // 
            // penToolStripMenuItem1
            // 
            resources.ApplyResources(this.penToolStripMenuItem1, "penToolStripMenuItem1");
            this.penToolStripMenuItem1.Name = "penToolStripMenuItem1";
            this.penToolStripMenuItem1.Click += new System.EventHandler(this.PenTool_Click);
            // 
            // fillToolStripMenuItem
            // 
            resources.ApplyResources(this.fillToolStripMenuItem, "fillToolStripMenuItem");
            this.fillToolStripMenuItem.Name = "fillToolStripMenuItem";
            this.fillToolStripMenuItem.Click += new System.EventHandler(this.FillTool_Click);
            // 
            // spongeToolStripMenuItem
            // 
            resources.ApplyResources(this.spongeToolStripMenuItem, "spongeToolStripMenuItem");
            this.spongeToolStripMenuItem.Name = "spongeToolStripMenuItem";
            this.spongeToolStripMenuItem.Click += new System.EventHandler(this.SpongeTool_Click);
            // 
            // eracerToolStripMenuItem
            // 
            resources.ApplyResources(this.eracerToolStripMenuItem, "eracerToolStripMenuItem");
            this.eracerToolStripMenuItem.Name = "eracerToolStripMenuItem";
            this.eracerToolStripMenuItem.Click += new System.EventHandler(this.EracerTool_Click);
            // 
            // mirrorSpriteToolStripMenuItem
            // 
            this.mirrorSpriteToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.horizontallyToolStripMenuItem1,
            this.verticallyToolStripMenuItem1});
            this.mirrorSpriteToolStripMenuItem.Name = "mirrorSpriteToolStripMenuItem";
            resources.ApplyResources(this.mirrorSpriteToolStripMenuItem, "mirrorSpriteToolStripMenuItem");
            // 
            // horizontallyToolStripMenuItem1
            // 
            this.horizontallyToolStripMenuItem1.CheckOnClick = true;
            this.horizontallyToolStripMenuItem1.Name = "horizontallyToolStripMenuItem1";
            resources.ApplyResources(this.horizontallyToolStripMenuItem1, "horizontallyToolStripMenuItem1");
            this.horizontallyToolStripMenuItem1.Click += new System.EventHandler(this.MirrorHorizontally_Click);
            // 
            // verticallyToolStripMenuItem1
            // 
            this.verticallyToolStripMenuItem1.CheckOnClick = true;
            this.verticallyToolStripMenuItem1.Name = "verticallyToolStripMenuItem1";
            resources.ApplyResources(this.verticallyToolStripMenuItem1, "verticallyToolStripMenuItem1");
            this.verticallyToolStripMenuItem1.Click += new System.EventHandler(this.MirrorVertically_Click);
            // 
            // viewToolStripMenuItem
            // 
            this.viewToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.previewWindowToolStripMenuItem,
            this.tilingToolStripMenuItem,
            this.tilsetEditorButton,
            this.lineGridToolStripMenuItem,
            this.sortPaletteToolStripMenuItem});
            this.viewToolStripMenuItem.Name = "viewToolStripMenuItem";
            resources.ApplyResources(this.viewToolStripMenuItem, "viewToolStripMenuItem");
            // 
            // previewWindowToolStripMenuItem
            // 
            this.previewWindowToolStripMenuItem.Name = "previewWindowToolStripMenuItem";
            resources.ApplyResources(this.previewWindowToolStripMenuItem, "previewWindowToolStripMenuItem");
            this.previewWindowToolStripMenuItem.Click += new System.EventHandler(this.PreviewWindow_Click);
            // 
            // tilingToolStripMenuItem
            // 
            this.tilingToolStripMenuItem.Name = "tilingToolStripMenuItem";
            resources.ApplyResources(this.tilingToolStripMenuItem, "tilingToolStripMenuItem");
            this.tilingToolStripMenuItem.Click += new System.EventHandler(this.Tiling_Click);
            // 
            // tilsetEditorButton
            // 
            this.tilsetEditorButton.Name = "tilsetEditorButton";
            resources.ApplyResources(this.tilsetEditorButton, "tilsetEditorButton");
            this.tilsetEditorButton.Click += new System.EventHandler(this.OpenTilesetEditor_Click);
            // 
            // lineGridToolStripMenuItem
            // 
            this.lineGridToolStripMenuItem.Name = "lineGridToolStripMenuItem";
            resources.ApplyResources(this.lineGridToolStripMenuItem, "lineGridToolStripMenuItem");
            this.lineGridToolStripMenuItem.Click += new System.EventHandler(this.LineGrid_Click);
            // 
            // sortPaletteToolStripMenuItem
            // 
            this.sortPaletteToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.byHueToolStripMenuItem,
            this.bySaturationToolStripMenuItem,
            this.byBrightnessToolStripMenuItem});
            this.sortPaletteToolStripMenuItem.Name = "sortPaletteToolStripMenuItem";
            resources.ApplyResources(this.sortPaletteToolStripMenuItem, "sortPaletteToolStripMenuItem");
            // 
            // byHueToolStripMenuItem
            // 
            this.byHueToolStripMenuItem.Name = "byHueToolStripMenuItem";
            resources.ApplyResources(this.byHueToolStripMenuItem, "byHueToolStripMenuItem");
            this.byHueToolStripMenuItem.Click += new System.EventHandler(this.SortByHue_Click);
            // 
            // bySaturationToolStripMenuItem
            // 
            this.bySaturationToolStripMenuItem.Name = "bySaturationToolStripMenuItem";
            resources.ApplyResources(this.bySaturationToolStripMenuItem, "bySaturationToolStripMenuItem");
            this.bySaturationToolStripMenuItem.Click += new System.EventHandler(this.SortBySaturation_Click);
            // 
            // byBrightnessToolStripMenuItem
            // 
            this.byBrightnessToolStripMenuItem.Name = "byBrightnessToolStripMenuItem";
            resources.ApplyResources(this.byBrightnessToolStripMenuItem, "byBrightnessToolStripMenuItem");
            this.byBrightnessToolStripMenuItem.Click += new System.EventHandler(this.SortByBrightness_Click);
            // 
            // otherToolStripMenuItem
            // 
            this.otherToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.reportBugToolStripMenuItem,
            this.featureRequestToolStripMenuItem,
            this.checkForUpdatesToolStripMenuItem});
            this.otherToolStripMenuItem.Name = "otherToolStripMenuItem";
            resources.ApplyResources(this.otherToolStripMenuItem, "otherToolStripMenuItem");
            // 
            // reportBugToolStripMenuItem
            // 
            this.reportBugToolStripMenuItem.Name = "reportBugToolStripMenuItem";
            resources.ApplyResources(this.reportBugToolStripMenuItem, "reportBugToolStripMenuItem");
            this.reportBugToolStripMenuItem.Click += new System.EventHandler(this.ReportBug_Click);
            // 
            // featureRequestToolStripMenuItem
            // 
            this.featureRequestToolStripMenuItem.Name = "featureRequestToolStripMenuItem";
            resources.ApplyResources(this.featureRequestToolStripMenuItem, "featureRequestToolStripMenuItem");
            this.featureRequestToolStripMenuItem.Click += new System.EventHandler(this.FeatureRequest_Click);
            // 
            // checkForUpdatesToolStripMenuItem
            // 
            this.checkForUpdatesToolStripMenuItem.Name = "checkForUpdatesToolStripMenuItem";
            resources.ApplyResources(this.checkForUpdatesToolStripMenuItem, "checkForUpdatesToolStripMenuItem");
            this.checkForUpdatesToolStripMenuItem.Click += new System.EventHandler(this.CheckForUpdates_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            resources.ApplyResources(this.helpToolStripMenuItem, "helpToolStripMenuItem");
            this.helpToolStripMenuItem.Click += new System.EventHandler(this.HelpButton_Click);
            // 
            // spritesLayout
            // 
            this.spritesLayout.AllowDrop = true;
            resources.ApplyResources(this.spritesLayout, "spritesLayout");
            this.spritesLayout.Name = "spritesLayout";
            // 
            // toolsPanelLayout
            // 
            this.toolsPanelLayout.AllowDrop = true;
            this.toolsPanelLayout.Controls.Add(this.drawButton);
            this.toolsPanelLayout.Controls.Add(this.FillBucket);
            this.toolsPanelLayout.Controls.Add(this.Eracer);
            this.toolsPanelLayout.Controls.Add(this.Sponge);
            resources.ApplyResources(this.toolsPanelLayout, "toolsPanelLayout");
            this.toolsPanelLayout.Name = "toolsPanelLayout";
            // 
            // drawButton
            // 
            resources.ApplyResources(this.drawButton, "drawButton");
            this.drawButton.BackColor = System.Drawing.Color.Transparent;
            this.drawButton.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.drawButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.drawButton.Image = global::SpriteEditor.Properties.Resources.pen;
            this.drawButton.Name = "drawButton";
            this.drawButton.TabStop = false;
            this.drawButton.Click += new System.EventHandler(this.PenTool_Click);
            // 
            // FillBucket
            // 
            resources.ApplyResources(this.FillBucket, "FillBucket");
            this.FillBucket.BackColor = System.Drawing.Color.Transparent;
            this.FillBucket.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.FillBucket.Cursor = System.Windows.Forms.Cursors.Hand;
            this.FillBucket.Image = global::SpriteEditor.Properties.Resources.bucket;
            this.FillBucket.Name = "FillBucket";
            this.FillBucket.TabStop = false;
            this.FillBucket.Click += new System.EventHandler(this.FillTool_Click);
            // 
            // Eracer
            // 
            resources.ApplyResources(this.Eracer, "Eracer");
            this.Eracer.BackColor = System.Drawing.Color.Transparent;
            this.Eracer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Eracer.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Eracer.Name = "Eracer";
            this.Eracer.TabStop = false;
            this.Eracer.Click += new System.EventHandler(this.EracerTool_Click);
            // 
            // Sponge
            // 
            resources.ApplyResources(this.Sponge, "Sponge");
            this.Sponge.BackColor = System.Drawing.Color.Transparent;
            this.Sponge.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Sponge.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Sponge.Image = global::SpriteEditor.Properties.Resources.sponge;
            this.Sponge.Name = "Sponge";
            this.Sponge.TabStop = false;
            this.Sponge.Click += new System.EventHandler(this.SpongeTool_Click);
            // 
            // colorRegionLayout
            // 
            this.colorRegionLayout.AllowDrop = true;
            this.colorRegionLayout.Controls.Add(this.ColorsRegion);
            this.colorRegionLayout.Controls.Add(this.CurrentColor_Left);
            this.colorRegionLayout.Controls.Add(this.CurrentColor_Right);
            this.colorRegionLayout.Controls.Add(this.colorSpectrum);
            this.colorRegionLayout.Controls.Add(this.colorBrightness);
            resources.ApplyResources(this.colorRegionLayout, "colorRegionLayout");
            this.colorRegionLayout.Name = "colorRegionLayout";
            // 
            // ColorsRegion
            // 
            resources.ApplyResources(this.ColorsRegion, "ColorsRegion");
            this.ColorsRegion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ColorsRegion.Name = "ColorsRegion";
            // 
            // CurrentColor_Left
            // 
            this.CurrentColor_Left.BackColor = System.Drawing.SystemColors.Control;
            this.CurrentColor_Left.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.CurrentColor_Left, "CurrentColor_Left");
            this.CurrentColor_Left.Name = "CurrentColor_Left";
            this.CurrentColor_Left.TabStop = false;
            // 
            // CurrentColor_Right
            // 
            this.CurrentColor_Right.BackColor = System.Drawing.SystemColors.Control;
            this.CurrentColor_Right.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.CurrentColor_Right, "CurrentColor_Right");
            this.CurrentColor_Right.Name = "CurrentColor_Right";
            this.CurrentColor_Right.TabStop = false;
            // 
            // colorSpectrum
            // 
            this.colorSpectrum.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.colorSpectrum, "colorSpectrum");
            this.colorSpectrum.Name = "colorSpectrum";
            this.colorSpectrum.TabStop = false;
            this.colorSpectrum.MouseClick += new System.Windows.Forms.MouseEventHandler(this.ColorPickerArea_Click);
            this.colorSpectrum.MouseMove += new System.Windows.Forms.MouseEventHandler(this.ColorPickerArea_Click);
            // 
            // toolTipStrip
            // 
            resources.ApplyResources(this.toolTipStrip, "toolTipStrip");
            this.toolTipStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.NewFrameButton,
            this.toolStripText});
            this.toolTipStrip.Name = "toolTipStrip";
            // 
            // NewFrameButton
            // 
            this.NewFrameButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this.NewFrameButton, "NewFrameButton");
            this.NewFrameButton.Name = "NewFrameButton";
            this.NewFrameButton.Click += new System.EventHandler(this.newFrame_Click);
            // 
            // toolStripText
            // 
            this.toolStripText.Name = "toolStripText";
            resources.ApplyResources(this.toolStripText, "toolStripText");
            // 
            // CanvasArea
            // 
            resources.ApplyResources(this.CanvasArea, "CanvasArea");
            this.CanvasArea.BackColor = System.Drawing.Color.Transparent;
            this.CanvasArea.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.CanvasArea.Name = "CanvasArea";
            this.CanvasArea.TabStop = false;
            this.CanvasArea.Paint += new System.Windows.Forms.PaintEventHandler(this.CanvasArea_Paint);
            this.CanvasArea.MouseClick += new System.Windows.Forms.MouseEventHandler(this.CanvasArea_MouseEvent);
            this.CanvasArea.MouseDown += new System.Windows.Forms.MouseEventHandler(this.CanvasArea_MouseDown);
            this.CanvasArea.MouseLeave += new System.EventHandler(this.CanvasArea_MouseLeave);
            this.CanvasArea.MouseMove += new System.Windows.Forms.MouseEventHandler(this.CanvasArea_MouseEvent);
            this.CanvasArea.MouseUp += new System.Windows.Forms.MouseEventHandler(this.CanvasArea_MouseUp);
            this.CanvasArea.Resize += new System.EventHandler(this.CanvasArea_Resize);
            // 
            // colorBrightness
            // 
            this.colorBrightness.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.colorBrightness, "colorBrightness");
            this.colorBrightness.Name = "colorBrightness";
            this.colorBrightness.TabStop = false;
            // 
            // Editor
            // 
            this.AllowDrop = true;
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.Controls.Add(this.toolTipStrip);
            this.Controls.Add(this.CanvasArea);
            this.Controls.Add(this.colorRegionLayout);
            this.Controls.Add(this.toolsPanelLayout);
            this.Controls.Add(this.spritesLayout);
            this.Controls.Add(this.menuStrip);
            this.MainMenuStrip = this.menuStrip;
            this.Name = "Editor";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Editor_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Editor_KeyDown);
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.toolsPanelLayout.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.drawButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FillBucket)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Eracer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Sponge)).EndInit();
            this.colorRegionLayout.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.CurrentColor_Left)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CurrentColor_Right)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorSpectrum)).EndInit();
            this.toolTipStrip.ResumeLayout(false);
            this.toolTipStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CanvasArea)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorBrightness)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox CurrentColor_Left;
        private System.Windows.Forms.PictureBox CurrentColor_Right;
        private System.Windows.Forms.PictureBox CanvasArea;
        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cursorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem penToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem eracerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem penToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem fillToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem spongeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem spriteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem paletteToolStripMenuItem;
        private PictureBox FillBucket;
        private PictureBox Eracer;
        private PictureBox drawButton;
        private PictureBox Sponge;
        private FlowLayoutPanel spritesLayout;
        private FlowLayoutPanel toolsPanelLayout;
        private FlowLayoutPanel colorRegionLayout;
        private ToolStripMenuItem editToolStripMenuItem1;
        private ToolStripMenuItem addNewFrameToolStripMenuItem;
        private ToolStripMenuItem otherToolStripMenuItem;
        private ToolStripMenuItem reportBugToolStripMenuItem;
        private ToolStripMenuItem featureRequestToolStripMenuItem;
        private ToolStripMenuItem checkForUpdatesToolStripMenuItem;
        private ToolStripMenuItem viewToolStripMenuItem;
        private ToolStripMenuItem previewWindowToolStripMenuItem;
        private ToolStripMenuItem sortPaletteToolStripMenuItem;
        private ToolStripMenuItem byHueToolStripMenuItem;
        private ToolStripMenuItem bySaturationToolStripMenuItem;
        private ToolStripMenuItem byBrightnessToolStripMenuItem;
        private ToolStripMenuItem resizeCanvasButton;
        private ToolStripMenuItem lineGridToolStripMenuItem;
        private ToolStripMenuItem flipSpriteToolStripMenuItem;
        private ToolStripMenuItem horizontallyToolStripMenuItem;
        private ToolStripMenuItem verticallyToolStripMenuItem;
        private ToolStripMenuItem flipRight90;
        private ToolStripMenuItem flipLeft90;
        private ToolStripMenuItem mirrorSpriteToolStripMenuItem;
        private ToolStripMenuItem horizontallyToolStripMenuItem1;
        private ToolStripMenuItem tilsetEditorButton;
        private ToolStripMenuItem verticallyToolStripMenuItem1;
        private FlowLayoutPanel ColorsRegion;
        private ToolStrip toolTipStrip;
        private ToolStripButton NewFrameButton;
        private ToolStripLabel toolStripText;
        private ToolStripMenuItem prefrencesToolStripMenuItem;
        private ToolStripMenuItem exitToolStripMenuItem;
        private ToolStripMenuItem tilingToolStripMenuItem;
        private PictureBox colorSpectrum;
        private PictureBox colorBrightness;
    }
}

